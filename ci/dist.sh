#!/bin/sh

# HACKS!!!!
#
# Remove VALAFLAGS="--target-glib=2.50"

apt install -y r-cran-ggplot2

SOURCE_TOP=$(pwd)
CLEAN_TOP=$(cd .. && pwd)/clean
mkdir -p ../clean
chmod -R ugo+w ../clean || exit 1
rm -rf ../clean || exit 1
mkdir -p ../clean || exit 1
rm -rf $SOURCE_TOP/dist || exit 1

cd $CLEAN_TOP
git clone $SOURCE_TOP . || exit 1

cd $SOURCE_TOP || exit 1
rm -rf ../libtarot-*.tar.gz ../libtarot-*/ libtarot-*.tar.gz
sh autogen.sh VALAFLAGS="--target-glib=2.50" || exit 1
./configure \
    CFLAGS="-Wall -Wextra -Werror -g $CFLAGS" \
    VALAFLAGS="--target-glib=2.50" \
    --prefix="$SOURCE_TOP/dist" \
    --enable-valgrind=yes \
    --enable-silent-rules=yes \
    || (cat config.log ; exit 1) \
    || exit 1
(cd lib && make -j 16 CFLAGS="-Wno-error -g $CFLAGS") || exit 1
make -j 16 distcheck || exit 1
make -j 8 check || exit 1
make -j 16 install install-html || exit 1
make indent || exit 1
rm -rf ../../libtarot-*.tar.gz ../../libtarot-*/
cp libtarot-*.tar.gz ../../ || exit 1
cd ../../ || exit 1
tar xf libtarot-*.tar.gz || exit 1
cd libtarot-* || exit 1
DIST_TOP=$(pwd)
cd $CLEAN_TOP || exit 1
OK="yes"
for source in $(find . -type f)
do
    case $source in
	*.po | *.pot)
	    echo "Ignoring PO file $source, as it has a date in it.";;
	*.md)
	    echo "Ignoring Markdown file $source, as it may be non-reproducibly generated by org-mode.";;
	*/.git/*);;
	*)
	    if diff $source $DIST_TOP/$source
	    then
		echo "$source: OK"
	    else
		echo "ERROR: $source has been modified since check-in, or ignored from the distribution.  Is it automatically generated?  Did you forget to indent it?  Did you forget to include it as distributed in Automake?"
		OK="no"
	    fi;;
	esac
done;
cd $DIST_TOP || exit 1
./configure && make maintainer-clean || exit 1
echo "Checking what files are maintainer-clean..."
for source in $(find . -type f)
do
    case $source in
	*.po | *.pot)
	    echo "Ignoring PO file $source, as it has a date in it.";;
	./po/en@quot.header | ./po/en@boldquot.header | ./po/Rules-quot | ./po/remove-potcdate.sin | ./po/boldquot.sed | ./po/quot.sed | ./po/insert-header.sin | ./po/Makefile.in.in)
	    echo "Ignoring Gettext garbage $source.";;
	./libopts/*)
	    echo "Ignoring embedded file $source, as it is not ours.";;
	./.tarball-version)
	    echo "Ignoring .tarball-version.";;
	./lib/* | ./snippet/*)
	    echo "Ignoring $source, some gnulib stuff.";;
	./m4/*)
	    echo "Ignoring autoconf macro $source.";;
	./git-version-gen | ./gitlog-to-changelog)
	    echo "Ignoring helper script $source.";;
	./compile | ./depcomp | ./config.rpath | ./config.sub | ./texinfo.tex | ./ar-lib | ./INSTALL | ./missing | ./test-driver | ./ltmain.sh | ./aclocal.m4 | ./ChangeLog | ./config.h.in | ./config.guess | ./Makefile.in | ./configure | ./install-sh | ./ABOUT-NLS)
	    echo "Ignoring required automake script $source.";;
	*)
	    if diff $source $CLEAN_TOP/$source
	    then
		echo "$source: OK"
	    else
		echo "ERROR: $source is not a maintainer-clean file, but it has been rebuilt or deleted since checkout.  If this file has been generated, add it to MAINTAINERCLEANFILES."
		OK="no"
	    fi;;
    esac
done;
if test "$OK" = "no"
then
    echo "Failure."
    exit 1
fi

## Nice!
cd $SOURCE_TOP || exit 1
mkdir -p $SOURCE_TOP/dist || exit 1
cp $SOURCE_TOP/.version \
   $SOURCE_TOP/dist/version || exit 1
cp libtarot-*.tar.gz $SOURCE_TOP/dist/ || exit 1

if test "x$CI_COMMIT_TAG" != "x"
then
    cd $SOURCE_TOP
    TARFILE=$(ls dist/libtarot-*.tar.gz)
    SHASUM=$(sha256sum $TARFILE | head -c 64)
    UPLOAD_STATUS=$(curl \
	      --request POST \
	      --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
	      --form \
	      "file=@$TARFILE" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/uploads")
    URL=$(echo "$UPLOAD_STATUS" | jq -r '.url')

    echo "Upload $TARFILE to $CI_API_V4_URL/projects/$CI_PROJECT_ID/uploads: $UPLOAD_STATUS"

    NAME="Tarot release"
    TAG_NAME="$CI_COMMIT_TAG"
    DESCRIPTION=$(git tag -l --format='%(contents)' "$CI_COMMIT_TAG")
    FULL_URL="${CI_PROJECT_URL}${URL}"
    LINK="{\"name\": \"$TARFILE\", \"url\": \"$FULL_URL\"}"
    ASSETS="{\"links\": [$LINK]}"
    DESCRIPTION_ESCAPED=$(echo "$DESCRIPTION" | jq -R --slurp '.')
    DATA="{\"name\": \"$NAME\", \"tag_name\": \"$TAG_NAME\", \"description\": $DESCRIPTION_ESCAPED, \"assets\": $ASSETS}"

    echo "POSTing $DATA to $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases"

    curl --header 'Content-Type: application/json' \
	 --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" \
	 --data "$DATA" \
	 --request POST \
	 "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases" || exit 1
    mkdir /root/.ssh || exit 1
    echo "$SSH_KEY" > /root/.ssh/id_rsa || exit 1
    echo "$SSH_KEY_PUB" > /root/.ssh/id_rsa.pub || exit 1
    echo "$SSH_KNOWN_HOSTS" > /root/.ssh/known_hosts || exit 1
    chmod go-rwx /root/.ssh/id_rsa || exit 1
    git config --global user.email "vivien@planete-kraus.eu" || exit 1
    git config --global user.name "Gitlab CI for the libtarot package" || exit 1
    git clone git@framagit.org:play-tarot/tarot-flatpak.git || exit 1
    cd tarot-flatpak || exit 1
    ./update-libtarot.sh "$FULL_URL" "$SHASUM" || exit 1
    git add .
    git commit -m"New version"
    git push
    cd .. || exit 1
    git clone git@framagit.org:play-tarot/tarot-deb.git || exit 1
    cd tarot-deb || exit 1
    ./update-libtarot.sh "$FULL_URL" "$(cat $SOURCE_TOP/.version)" || exit 1
    git add .
    git commit -m"New version"
    git push
    cd .. || exit 1
    git clone git@framagit.org:play-tarot/tarot-mingw.git || exit 1
    cd tarot-mingw || exit 1
    ./update-libtarot.sh "$FULL_URL" || exit 1
    git add .
    git commit -m"New version"
    git push
fi
