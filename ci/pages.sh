#!/bin/sh

VERSION=$(cat dist/version)

cp dist/libtarot-$VERSION.tar.gz . || exit 1
tar xf libtarot-$VERSION.tar.gz || exit 1
mkdir build || exit 1
cd build/
../libtarot-$VERSION/configure \
    --prefix="$PWD/../public" \
    --enable-silent-rules=yes \
    --without-cairo \
    CFLAGS="-g -fprofile-arcs -ftest-coverage" \
    || (cat config.log; exit 1)
make -j 16 install-html || exit 1
make distclean || exit 1
cd .. || exit 1

cp libtarot-$VERSION/index.html public/ || exit 1

if test "x$CI_PAGES_URL" = "x"
then
    export CI_PAGES_URL="https://play-tarot.frama.io/libtarot"
fi

for link in $(wget -O- "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases" | jq -r '.[0].assets.links[].url')
do
    case $link in
	*/libtarot-*.*.*.tar.gz)
	    wget -O public/libtarot-latest.tar.gz "$link" || exit 1;;
	*)
	    echo "Unknown link '$link'" ; exit 1;;
    esac
done
