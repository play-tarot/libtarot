#!/bin/sh

VERSION=$(cat dist/version)

cp dist/libtarot-$VERSION.tar.gz . || exit 1
tar xf libtarot-$VERSION.tar.gz || exit 1
mkdir build || exit 1
cd build/
../libtarot-$VERSION/configure \
    --enable-silent-rules=yes \
    --enable-valgrind=yes \
    --without-cairo \
    || (cat config.log; exit 1)
make -j 16 check || exit 1
make distclean || exit 1
