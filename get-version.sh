#!/bin/sh

PACKAGE_VERSION=$(./git-version-gen .tarball-version \
		      | sed 's|/[[:digit:]]\.[[:digit:]]\.[[:digit:]]||g')
LIBTOOL_VERSION=$(./git-version-gen .tarball-version \
		      | sed 's|.*/\([[:digit:]]\.[[:digit:]]\.[[:digit:]]\).*|\1|g' \
		      | sed 's/\./:/g')

case $1 in
    package)
	echo -n "$PACKAGE_VERSION";;
    libtool)
	echo -n "$LIBTOOL_VERSION";;
    *)
	echo -n "Usage: ./get-version.sh (package|libtool)" ; exit 1;;
esac
