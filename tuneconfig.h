/**
 * file tuneconfig.h
 *
 * Copyright (C) 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TUNECONFIG_INCLUDED
#define H_TUNECONFIG_INCLUDED
#define TUNE_ITERATIONS 100
#define TUNE_PARAMETER 0.000828
#define TUNE_SIMULATION_RANDOM 0.000000
#define TUNE_SIMULATION_AGREED 0.781867
#endif /* not H_TUNECONFIG_INCLUDED */
/*  */
