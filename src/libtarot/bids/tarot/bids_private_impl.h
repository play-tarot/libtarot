/**
 * file tarot/bids.c libtarot code for the management of the round of
 * bids.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/bid_private.h>
#include <assert.h>

static size_t
bids_get_n_bids (const TarotBids * bids)
{
  return bids->n_bids;
}

static void
bids_set_n_bids (TarotBids * bids, size_t n)
{
  bids->n_bids = n;
}

static int
bids_get_has_started (const TarotBids * bids)
{
  return bids->has_started;
}

static void
bids_set_has_started (TarotBids * bids, int has_started)
{
  bids->has_started = has_started;
}

static TarotPlayer
bids_get_taker (const TarotBids * bids)
{
  return bids->taker;
}

static void
bids_set_taker (TarotBids * bids, TarotPlayer taker)
{
  bids->taker = taker;
}

static TarotBid
bids_get_bid (const TarotBids * bids, size_t i)
{
  return bids->bids[i];
}

static void
bids_set_bid (TarotBids * bids, size_t i, TarotBid bid)
{
  bids->bids[i] = bid;
}

static void
bids_initialize (TarotBids * bids)
{
  bids_set_n_bids (bids, 0);
  bids_set_has_started (bids, 0);
  bids_set_taker (bids, 0);
}

static inline int
bids_copy (TarotBids * dest, const TarotBids * source)
{
  bids_set_n_bids (dest, bids_get_n_bids (source));
  bids_set_has_started (dest, bids_get_has_started (source));
  if (bids_get_has_started (source))
    {
      bids_set_taker (dest, bids_get_taker (source));
    }
  memcpy (dest->bids, source->bids, source->n_bids * sizeof (TarotBid));
  return 0;
}

static int
bids_start (TarotBids * bids)
{
  int error = bids_get_has_started (bids);
  if (error == 0)
    {
      bids_set_has_started (bids, 1);
    }
  return error;
}

static int
bids_has_next (const TarotBids * bids, size_t n_players)
{
  return (bids_get_has_started (bids) && bids_get_n_bids (bids) < n_players);
}

static TarotPlayer
bids_next (const TarotBids * bids)
{
  return bids_get_n_bids (bids);
}

static int
bids_has_taker (const TarotBids * bids, size_t n_players)
{
  TarotPlayer taker = bids_get_taker (bids);
  return (bids_get_has_started (bids)
          && taker < n_players
          && taker < bids_get_n_bids (bids)
          && bids_get_bid (bids, taker) != TAROT_PASS);
}

static TarotPlayer
bids_taker (const TarotBids * bids)
{
  return bids_get_taker (bids);
}

static TarotBid
bids_contract (const TarotBids * bids)
{
  TarotPlayer taker = bids_get_taker (bids);
  if (taker < bids_get_n_bids (bids))
    {
      return bids_get_bid (bids, taker);
    }
  /* There is no bids yet: taker is invalid, but we have to return
     TAROT_PASS because tarot_game_hint_bid expects us to do so */
  return TAROT_PASS;
}

static int
bids_discard_allowed (const TarotBids * bids, size_t n_players)
{
  return (bids_has_taker (bids, n_players)
          && tarot_bid_discard_allowed (bids_contract (bids)));
}

static int
bids_discard_counted (const TarotBids * bids, size_t n_players)
{
  return (bids_has_taker (bids, n_players)
          && tarot_bid_discard_counted (bids_contract (bids)));
}

static int
bids_multiplier (const TarotBids * bids, size_t n_players)
{
  if (!bids_has_taker (bids, n_players))
    {
      return 0;
    }
  return tarot_bid_multiplier (bids_contract (bids));
}

static int
bids_check (const TarotBids * bids, TarotBid next, size_t n_players)
{
  int superior = 0;
  int ret;
  if (!bids_has_next (bids, n_players))
    {
      return 0;
    }
  ret = tarot_bid_check (bids_contract (bids), next, &superior);
  (void) superior;
  return ret;
}

static int
bids_add (TarotBids * bids, TarotBid next, size_t n_players)
{
  int overriden;
  int ok;
  if (!bids_has_next (bids, n_players))
    {
      return 1;
    }
  ok = tarot_bid_check (bids_contract (bids), next, &overriden);
  if (!ok)
    {
      return 1;
    }
  if (overriden)
    {
      bids_set_taker (bids, bids_get_n_bids (bids));
    }
  bids_set_bid (bids, bids_get_n_bids (bids), next);
  bids_set_n_bids (bids, bids_get_n_bids (bids) + 1);
  if (next == TAROT_DOUBLE_KEEP)
    {
      while (bids_has_next (bids, n_players))
        {
          int error = bids_add (bids, TAROT_PASS, n_players);
          assert (error == 0);
          (void) error;
        }
    }
  return 0;
}

#include <tarot/bid_private_impl.h>
