/**
 * file pool.h Tarot pool allocator
 *
 * Copyright (C) 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_POOL_INCLUDED
#define H_TAROT_POOL_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static inline int
    align_head (size_t *available, char **head, size_t alignment)
  {
    size_t offset = (size_t) *head;
    size_t padding = (alignment - (offset % alignment)) % alignment;
    if (*available < padding)
      {
        return 1;
      }
     *available -= padding;
    *head += padding;
    return 0;
  }

  static inline void *talloc (size_t *available, char **head, size_t sz,
                              size_t alignment)
  {
    int align_error = align_head (available, head, alignment);
    void *ret = NULL;
    if (align_error)
      {
        return NULL;
      }
    if (*available < sz)
      {
        return NULL;
      }
    ret = *head;
    *available -= sz;
    *head += sz;
    return ret;
  }

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* H_TAROT_POOL_INCLUDED */
