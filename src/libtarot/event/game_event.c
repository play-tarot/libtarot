/**
 * file game_event.c
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/game_event_private.h>
#include <string.h>
#include <stdlib.h>
#include "xalloc.h"
#include <assert.h>

TarotGameEventT
tarot_game_event_type (const TarotGameEvent * event)
{
  return event_type (event);
}

size_t
tarot_game_event_construct_setup (size_t max_mem, char *mem,
                                  size_t *alignment, size_t n_players,
                                  int with_call)
{
  return event_construct_setup (max_mem, mem, alignment, n_players,
                                with_call);
}

TarotGameEvent *
tarot_game_event_alloc_setup (size_t n_players, int with_call)
{
  size_t alignment;
  size_t required = event_construct_setup (0, NULL, &alignment, n_players,
                                           with_call);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_setup (required, mem, &alignment, n_players, with_call);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_deal (size_t max_mem, char *mem, size_t *alignment,
                                 TarotPlayer myself, size_t n_cards,
                                 const TarotCard * cards)
{
  return event_construct_deal (max_mem, mem, alignment, myself, n_cards,
                               cards);
}

TarotGameEvent *
tarot_game_event_alloc_deal (TarotPlayer myself, size_t n_cards,
                             const TarotCard * cards)
{
  size_t alignment;
  size_t required =
    event_construct_deal (0, NULL, &alignment, myself, n_cards,
                          cards);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_deal (required, mem, &alignment, myself, n_cards, cards);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_deal_all (size_t max_mem, char *mem,
                                     size_t *alignment, size_t n_owners,
                                     const TarotPlayer * owners)
{
  return event_construct_deal_all (max_mem, mem, alignment, n_owners, owners);
}

TarotGameEvent *
tarot_game_event_alloc_deal_all (size_t n_owners, const TarotPlayer * owners)
{
  size_t alignment;
  size_t required = event_construct_deal_all (0, NULL, &alignment, n_owners,
                                              owners);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_deal_all (required, mem, &alignment, n_owners, owners);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_deal_all_random (size_t max_mem, char *mem,
                                            size_t *alignment,
                                            size_t n_players,
                                            size_t seed_size,
                                            const void *seed)
{
  return event_construct_deal_all_random (max_mem, mem, alignment, n_players,
                                          seed_size, seed);
}

TarotGameEvent *
tarot_game_event_alloc_deal_all_random (size_t n_players, size_t seed_size,
                                        const void *seed)
{
  size_t alignment;
  size_t required = event_construct_deal_all_random (0, NULL, &alignment,
                                                     n_players, seed_size,
                                                     seed);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_deal_all_random (required, mem, &alignment,
                                     n_players, seed_size, seed);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_bid (size_t max_mem, char *mem, size_t *alignment,
                                TarotBid bid)
{
  return event_construct_bid (max_mem, mem, alignment, bid);
}

TarotGameEvent *
tarot_game_event_alloc_bid (TarotBid bid)
{
  size_t alignment;
  size_t required = event_construct_bid (0, NULL, &alignment, bid);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = event_construct_bid (required, mem, &alignment, bid);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_decl (size_t max_mem, char *mem, size_t *alignment,
                                 int decl)
{
  return event_construct_decl (max_mem, mem, alignment, decl);
}

TarotGameEvent *
tarot_game_event_alloc_decl (int decl)
{
  size_t alignment;
  size_t required = event_construct_decl (0, NULL, &alignment, decl);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = event_construct_decl (required, mem, &alignment, decl);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_call (size_t max_mem, char *mem, size_t *alignment,
                                 TarotCard call)
{
  return event_construct_call (max_mem, mem, alignment, call);
}

TarotGameEvent *
tarot_game_event_alloc_call (TarotCard call)
{
  size_t alignment;
  size_t required = event_construct_call (0, NULL, &alignment, call);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = event_construct_call (required, mem, &alignment, call);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_dog (size_t max_mem, char *mem, size_t *alignment,
                                size_t n_cards, const TarotCard * cards)
{
  return event_construct_dog (max_mem, mem, alignment, n_cards, cards);
}

TarotGameEvent *
tarot_game_event_alloc_dog (size_t n_cards, const TarotCard * cards)
{
  size_t alignment;
  size_t required = event_construct_dog (0, NULL, &alignment, n_cards, cards);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = event_construct_dog (required, mem, &alignment, n_cards, cards);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_discard (size_t max_mem, char *mem,
                                    size_t *alignment, size_t n_cards,
                                    const TarotCard * cards)
{
  return event_construct_discard (max_mem, mem, alignment, n_cards, cards);
}

TarotGameEvent *
tarot_game_event_alloc_discard (size_t n_cards, const TarotCard * cards)
{
  size_t alignment;
  size_t required =
    event_construct_discard (0, NULL, &alignment, n_cards, cards);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_discard (required, mem, &alignment, n_cards, cards);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_handful (size_t max_mem, char *mem,
                                    size_t *alignment, size_t n_cards,
                                    const TarotCard * cards)
{
  return event_construct_handful (max_mem, mem, alignment, n_cards, cards);
}

TarotGameEvent *
tarot_game_event_alloc_handful (size_t n_cards, const TarotCard * cards)
{
  size_t alignment;
  size_t required =
    event_construct_handful (0, NULL, &alignment, n_cards, cards);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    event_construct_handful (required, mem, &alignment, n_cards, cards);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_card (size_t max_mem, char *mem, size_t *alignment,
                                 TarotCard card)
{
  return event_construct_card (max_mem, mem, alignment, card);
}

TarotGameEvent *
tarot_game_event_alloc_card (TarotCard card)
{
  size_t alignment;
  size_t required = event_construct_card (0, NULL, &alignment, card);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = event_construct_card (required, mem, &alignment, card);
  assert (unused == required);
  (void) unused;
  return mem;
}

size_t
tarot_game_event_construct_copy (size_t max_mem, char *mem, size_t *alignment,
                                 const TarotGameEvent * event)
{
  switch (event_type (event))
    {
    case TAROT_SETUP_EVENT:
      return event_construct_setup (max_mem, mem, alignment,
                                    event->u.setup.n_players,
                                    event->u.setup.with_call);
    case TAROT_DEAL_EVENT:
      return event_construct_deal (max_mem, mem, alignment, event->u.deal,
                                   event->n, event->data);
    case TAROT_DEAL_ALL_EVENT:
      return event_construct_deal_all (max_mem, mem, alignment, event->n,
                                       event->data);
    case TAROT_BID_EVENT:
      return event_construct_bid (max_mem, mem, alignment, event->u.bid);
    case TAROT_DECL_EVENT:
      return event_construct_decl (max_mem, mem, alignment, event->u.decl);
    case TAROT_CALL_EVENT:
      return event_construct_call (max_mem, mem, alignment, event->u.call);
    case TAROT_DOG_EVENT:
      return event_construct_dog (max_mem, mem, alignment, event->n,
                                  event->data);
    case TAROT_DISCARD_EVENT:
      return event_construct_discard (max_mem, mem, alignment, event->n,
                                      event->data);
    case TAROT_HANDFUL_EVENT:
      return event_construct_handful (max_mem, mem, alignment, event->n,
                                      event->data);
    case TAROT_CARD_EVENT:
      return event_construct_card (max_mem, mem, alignment, event->u.card);
    default:
      break;
    }
  return 0;
}

TarotGameEvent *
tarot_game_event_dup (const TarotGameEvent * source)
{
  size_t alignment;
  size_t required =
    tarot_game_event_construct_copy (0, NULL, &alignment, source);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    tarot_game_event_construct_copy (required, mem, &alignment, source);
  assert (unused == required);
  (void) unused;
  return mem;
}

void
tarot_game_event_free (TarotGameEvent * game_event)
{
  free (game_event);
}

TarotGameEventError
tarot_game_event_get_setup (const TarotGameEvent * event,
                            size_t *n_players, int *with_call)
{
  return event_get_setup (event, n_players, with_call);
}

TarotGameEventError
tarot_game_event_get_deal (const TarotGameEvent * event,
                           TarotPlayer * myself,
                           size_t *n_cards,
                           size_t start, size_t max, TarotCard * cards)
{
  return event_get_deal (event, myself, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_deal_alloc (const TarotGameEvent * event,
                                 TarotPlayer * whom, size_t *n_cards,
                                 TarotCard ** cards)
{
  TarotGameEventError ret = event_get_deal (event, whom, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_deal (event, whom, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_deal_all (const TarotGameEvent * event,
                               size_t *n_owners,
                               size_t start, size_t max, TarotPlayer * owners)
{
  return event_get_deal_all (event, n_owners, start, max, owners);
}

TarotGameEventError
tarot_game_event_get_deal_all_alloc (const TarotGameEvent * event,
                                     size_t *n_owners, TarotPlayer ** owners)
{
  TarotGameEventError ret = event_get_deal_all (event, n_owners, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_owners_2;
      *owners = xmalloc (*n_owners * sizeof (TarotCard));
      ret2 = event_get_deal_all (event, &n_owners_2, 0, *n_owners, *owners);
      assert (ret2 == ret);
      assert (*n_owners == n_owners_2);
      (void) ret;
      (void) n_owners_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_bid (const TarotGameEvent * event, TarotBid * bid)
{
  return event_get_bid (event, bid);
}

TarotGameEventError
tarot_game_event_get_decl (const TarotGameEvent * event, int *decl)
{
  return event_get_decl (event, decl);
}

TarotGameEventError
tarot_game_event_get_call (const TarotGameEvent * event, TarotCard * call)
{
  return event_get_call (event, call);
}

TarotGameEventError
tarot_game_event_get_dog (const TarotGameEvent * event,
                          size_t *n_cards,
                          size_t start, size_t max, TarotCard * cards)
{
  return event_get_dog (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_dog_alloc (const TarotGameEvent * event, size_t *n_cards,
                                TarotCard ** cards)
{
  TarotGameEventError ret = event_get_dog (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_dog (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_discard (const TarotGameEvent * event,
                              size_t *n_cards,
                              size_t start, size_t max, TarotCard * cards)
{
  return event_get_discard (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_discard_alloc (const TarotGameEvent * event,
                                    size_t *n_cards, TarotCard ** cards)
{
  TarotGameEventError ret = event_get_discard (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_discard (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_handful (const TarotGameEvent * event,
                              size_t *n_cards,
                              size_t start, size_t max, TarotCard * cards)
{
  return event_get_handful (event, n_cards, start, max, cards);
}

TarotGameEventError
tarot_game_event_get_handful_alloc (const TarotGameEvent * event,
                                    size_t *n_cards, TarotCard ** cards)
{
  TarotGameEventError ret = event_get_handful (event, n_cards, 0, 0, NULL);
  if (ret == TAROT_EVENT_OK)
    {
      TarotGameEventError ret2;
      size_t n_cards_2;
      *cards = xmalloc (*n_cards * sizeof (TarotCard));
      ret2 = event_get_handful (event, &n_cards_2, 0, *n_cards, *cards);
      assert (ret2 == ret);
      assert (*n_cards == n_cards_2);
      (void) ret;
      (void) n_cards_2;
    }
  return ret;
}

TarotGameEventError
tarot_game_event_get_card (const TarotGameEvent * event, TarotCard * card)
{
  return event_get_card (event, card);
}

#include <tarot/game_event_private_impl.h>
