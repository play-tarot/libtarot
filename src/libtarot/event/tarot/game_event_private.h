/**
 * file tarot/game_event_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_EVENT_PRIVATE_INCLUDED
#define H_TAROT_GAME_EVENT_PRIVATE_INCLUDED

#include <tarot/game_event.h>
#include <tarot_private.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */
  static size_t event_size (size_t n_data);
  static void event_copy (TarotGameEvent * dest,
                          const TarotGameEvent * source);

  static TarotGameEventT event_type (const TarotGameEvent * event);
  static size_t event_construct_setup (size_t max_mem, char *mem,
                                       size_t *alignment, size_t n_players,
                                       int with_call);
  static size_t event_construct_deal (size_t max_mem, char *mem,
                                      size_t *alignment, TarotPlayer myself,
                                      size_t n_cards,
                                      const TarotCard * cards);
  static size_t event_construct_deal_all (size_t max_mem, char *mem,
                                          size_t *alignment, size_t n_owners,
                                          const TarotPlayer * owners);
  static size_t event_construct_deal_all_random (size_t max_mem, char *mem,
                                                 size_t *alignment,
                                                 size_t n_players,
                                                 size_t seed_size,
                                                 const void *seed);
  static size_t event_construct_bid (size_t max_mem, char *mem,
                                     size_t *alignment, TarotBid bid);
  static size_t event_construct_decl (size_t max_mem, char *mem,
                                      size_t *alignment, int slam);
  static size_t event_construct_call (size_t max_mem, char *mem,
                                      size_t *alignment, TarotCard call);
  static size_t event_construct_dog (size_t max_mem, char *mem,
                                     size_t *alignment, size_t n_cards,
                                     const TarotCard * cards);
  static size_t event_construct_discard (size_t max_mem, char *mem,
                                         size_t *alignment, size_t n_cards,
                                         const TarotCard * cards);
  static size_t event_construct_handful (size_t max_mem, char *mem,
                                         size_t *alignment, size_t n_cards,
                                         const TarotCard * cards);
  static size_t event_construct_card (size_t max_mem, char *mem,
                                      size_t *alignment, TarotCard c);

  static void event_set_setup (TarotGameEvent * event, size_t n_players,
                               int with_call);
  static void event_set_deal (TarotGameEvent * event, TarotPlayer myself,
                              size_t n_cards, const TarotCard * cards);
  static void event_set_deal_all (TarotGameEvent * event, size_t n_owners,
                                  const TarotPlayer * owners);
  static void event_set_deal_all_random (TarotGameEvent * event,
                                         size_t n_players, size_t seed_size,
                                         const void *seed);
  static void event_set_bid (TarotGameEvent * event, TarotBid bid);
  static void event_set_decl (TarotGameEvent * event, int decl);
  static void event_set_call (TarotGameEvent * event, TarotCard call);
  static void event_set_dog (TarotGameEvent * event, size_t n_cards,
                             const TarotCard * cards);
  static void event_set_discard (TarotGameEvent * event, size_t n_cards,
                                 const TarotCard * cards);
  static void event_set_handful (TarotGameEvent * event, size_t n_cards,
                                 const TarotCard * cards);
  static void event_set_card (TarotGameEvent * event, TarotCard card);

  static TarotGameEventError event_get_setup (const TarotGameEvent * event,
                                              size_t *n_players,
                                              int *with_call);
  static TarotGameEventError event_get_deal (const TarotGameEvent * event,
                                             TarotPlayer * myself,
                                             size_t *n_cards, size_t start,
                                             size_t max, TarotCard * cards);
  static TarotGameEventError event_get_deal_all (const TarotGameEvent * event,
                                                 size_t *n_owners,
                                                 size_t start, size_t max,
                                                 TarotPlayer * owners);
  static TarotGameEventError event_get_bid (const TarotGameEvent * event,
                                            TarotBid * bid);
  static TarotGameEventError event_get_decl (const TarotGameEvent * event,
                                             int *decl);
  static TarotGameEventError event_get_call (const TarotGameEvent * event,
                                             TarotCard * call);
  static TarotGameEventError event_get_dog (const TarotGameEvent * event,
                                            size_t *n_cards, size_t start,
                                            size_t max, TarotCard * cards);
  static TarotGameEventError event_get_discard (const TarotGameEvent * event,
                                                size_t *n_cards, size_t start,
                                                size_t max,
                                                TarotCard * cards);
  static TarotGameEventError event_get_handful (const TarotGameEvent * event,
                                                size_t *n_cards, size_t start,
                                                size_t max,
                                                TarotCard * cards);
  static TarotGameEventError event_get_card (const TarotGameEvent * event,
                                             TarotCard * card);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_GAME_EVENT_PRIVATE_INCLUDED */
