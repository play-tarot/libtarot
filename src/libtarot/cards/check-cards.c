/**
 * file check-cards.c Check the gperf hash function.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int
main ()
{
  char buffer_too_short[1];
  char buffer_just_right[3];
  char buffer_large_enough[4];
  size_t needed;
  needed = tarot_card_to_string (TAROT_EXCUSE, 0, NULL);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 1, buffer_too_short);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_too_short[0] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 3, buffer_just_right);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_just_right[2] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  needed = tarot_card_to_string (TAROT_EXCUSE, 4, buffer_large_enough);
  if (needed != 3)
    {
      fprintf (stderr, "The cards test failed: %lu != 3.\n", needed);
      abort ();
    }
  if (buffer_large_enough[3] != '\0')
    {
      fprintf (stderr, "The cards test failed: buffer not nul-terminated.\n");
      abort ();
    }
  if (strcmp (buffer_large_enough, "EXC") != 0)
    {
      fprintf (stderr, "The cards test failed: buffer set to garbage.\n");
      abort ();
    }
  if (tarot_string_to_card ("EXC") != TAROT_EXCUSE)
    {
      fprintf (stderr, "The cards test failed: the excuse is %u != %u.\n",
               tarot_string_to_card ("EXC"), TAROT_EXCUSE);
      abort ();
    }
  if (tarot_string_to_card ("AH") != 0)
    {
      fprintf (stderr,
               "The cards test failed: the ace of hearts is %u != %u.\n",
               tarot_string_to_card ("AH"), 0);
      abort ();
    }
  if (tarot_string_to_card ("garbage") < 78)
    {
      fprintf (stderr, "The cards test failed: garbage recognized as %u.\n",
               tarot_string_to_card ("garbage"));
      abort ();
    }
  fprintf (stderr, "Cards test: OK\n");
  return 0;
}
