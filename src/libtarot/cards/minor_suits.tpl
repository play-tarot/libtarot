[+ AutoGen5 template sh +]
echo "autogen definitions card;"
echo ""
i=0
NUMBERS="ace two three four five six seven eight nine ten jack knight queen king"
INITIALS="A $(seq 2 10) J C Q K"
[+ FOR suit +]
for number in $(seq 1 14)
do
    NUMBER=$(printf "%s\n" $NUMBERS | tail -n+$number | head -n 1)
    INITIAL=$(printf "%s\n" $INITIALS | tail -n+$number | head -n 1)
    echo "card = {"
    echo "  id = $i;"
    echo "  suit = TAROT_[+ (string-upcase (get "name")) +];"
    echo "  number = $number;"
    echo "  name = \"$NUMBER of [+ name +]\";"
    echo "  short = \"${INITIAL}[+ letter +]\";"
    echo "};"
    i=$((i + 1))
done
[+ ENDFOR +]

NUMBERS="two three four five six seven eight nine ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen twenty twenty-one"
INITIALS="$(seq 2 21)"
echo "card = {"
echo "  id = $i;"
echo "  suit = TAROT_TRUMPS;"
echo "  number = 1;"
echo "  name = \"petit\";"
echo "  short = \"1T\";"
echo "};";
i=$((i + 1))
for initial in $(seq 2 21)
do
    NUMBER=$(printf "%s\n" $NUMBERS | tail -n+$number | head -n 1)
    echo "card = {"
    echo "  id = $i;"
    echo "  suit = TAROT_TRUMPS;"
    echo "  number = $initial;"
    echo "  name = \"$NUMBER of trumps\";"
    echo "  short = \"${initial}T\";"
    echo "};"
    i=$((i + 1)) 
done
echo "card = {"
echo "  id = $i;"
echo "  name = \"excuse\";"
echo "  short = \"EXC\";"
echo "};";
i=$((i + 1))
