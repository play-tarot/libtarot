/* *INDENT-OFF* */
[+ AutoGen5 template h +]
/* *INDENT-ON* */
/**
 * file cards_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef H_TAROT_CARDS_PRIVATE_INCLUDED
#define H_TAROT_CARDS_PRIVATE_INCLUDED
#include <stddef.h>
#include <tarot/cards.h>
#define N_(String) (String)
#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /* *INDENT-OFF* */
  [+ FOR card +]
  /** Index of card [+ name +] */
  #define TAROT_[+ short +] [+ id +]
  [+ ENDFOR card +]
  /* *INDENT-ON* */

#define TAROT_PETIT TAROT_1T
#define TAROT_TWENTYONE TAROT_21T

  typedef char static_assertion_excuse_eq_77[(TAROT_EXCUSE == TAROT_EXC)
                                             ? 1 : -1];

  static int of (TarotNumber n, TarotSuit s, TarotCard * c);
  static int decompose (TarotCard c, TarotNumber * n, TarotSuit * s);
  static TarotCard card_parse (const char *text, char **end);
  static TarotCard card_parse_c (const char *text, char **end);
  static size_t card_to_string (TarotCard c, size_t max, char *dest);
  static size_t card_to_string_c (TarotCard c, size_t max, char *dest);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_CARDS_PRIVATE_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
