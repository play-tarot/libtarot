/* *INDENT-OFF* */
[+ AutoGen5 template h +]
/* *INDENT-ON* */
/**
 * file cards_private_impl.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED
#define H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED
#include <stddef.h>
#include <tarot/cards_private.h>
#include <string.h>
#include <assert.h>
#include <gettext.h>
#define _(String) dgettext (PACKAGE, String)

static inline int
of (TarotNumber n, TarotSuit s, TarotCard * c)
{
    /* *INDENT-OFF* */
    [+ FOR card +][+ CASE short +][+ == "EXC" +][+ * +]
    if (n == [+ number +] && s == [+ suit +])
      {
	*c = [+ id +];
	return 0;
      }[+ ESAC +][+ ENDFOR card +]
    /* *INDENT-ON* */
  return 1;
}

static inline int
decompose (TarotCard c, TarotNumber * n, TarotSuit * s)
{
    /* *INDENT-OFF* */
    [+ FOR card +][+ CASE short +][+ == "EXC" +]
    if (c == [+ id +])
      {
	return 1;
      }[+ * +]
    if (c == [+ id +])
      {
	*n = [+ number +];
	*s = [+ suit +];
	return 0;
      }[+ ESAC +][+ ENDFOR card +]
    /* *INDENT-ON* */
  return 1;
}

static inline TarotCard
card_parse (const char *text, char **end)
{
  size_t len = 0;
  len = strlen (text);
  /* *INDENT-OFF* */
  [+ FOR card +]
  if (len >= strlen (_ ("[+ short +]"))
      && strncmp (text, _ ("[+ short +]"), strlen (_ ("[+ short +]"))) == 0)
    {
      if (end != NULL)
	{
	  *end = (char *) text + strlen (_ ("[+ short +]"));
	}
      return [+ id +];
    }
  else if (len < strlen (_ ("[+ short +]"))
	   && strncmp (text, _ ("[+ short +]"), len) == 0)
    {
      if (end != NULL)
	{
	  *end = (char *) text + len;
	  assert (**end == '\0');
	}
      return (TarotCard) (-1);
    }
  [+ ENDFOR card +]
  /* *INDENT-ON* */
  if (end != NULL)
    {
      *end = (char *) text;
    }
  return (TarotCard) (-1);
}

static inline TarotCard
card_parse_c (const char *text, char **end)
{
  size_t len = 0;
  len = strlen (text);
  /* *INDENT-OFF* */
  [+ FOR card +]
  if (len >= strlen ("[+ short +]")
      && strncmp (text, "[+ short +]", strlen ("[+ short +]")) == 0)
    {
      if (end != NULL)
	{
	  *end = (char *) text + strlen ("[+ short +]");
	}
      return [+ id +];
    }
  else if (len < strlen ("[+ short +]")
	   && strncmp (text, "[+ short +]", len) == 0)
    {
      if (end != NULL)
	{
	  *end = (char *) text + len;
	  assert (**end == '\0');
	}
      return (TarotCard) (-1);
    }
  [+ ENDFOR card +]
  /* *INDENT-ON* */
  if (end != NULL)
    {
      *end = (char *) text;
    }
  return (TarotCard) (-1);
}

static inline size_t
card_to_string_c (TarotCard c, size_t max, char *dest)
{
    /* *INDENT-OFF* */
    [+ FOR card +]
    if (c == [+ id +])
      {
	size_t required = strlen ("[+ short +]");
	if (max > required)
	  {
	    strncpy (dest, "[+ short +]", required);
	    dest[required] = '\0';
	  }
	else if (max != 0)
	  {
	    strncpy (dest, "[+ short +]", max - 1);
	    dest[max - 1] = '\0';
	  }
	return required;
      }
    [+ ENDFOR card +]
    /* *INDENT-ON* */
  if (max != 0)
    {
      *dest = '\0';
    }
  return 0;
}

static inline size_t
card_to_string (TarotCard c, size_t max, char *dest)
{
    /* *INDENT-OFF* */
    [+ FOR card +]
    if (c == [+ id +])
      {
	size_t required = strlen (_ ("[+ short +]"));
	if (max > required)
	  {
	    strncpy (dest, _ ("[+ short +]"), required);
	    dest[required] = '\0';
	  }
	else if (max != 0)
	  {
	    strncpy (dest, _ ("[+ short +]"), max - 1);
	    dest[max - 1] = '\0';
	  }
	return required;
      }
    [+ ENDFOR card +]
    /* *INDENT-ON* */
  if (max != 0)
    {
      *dest = '\0';
    }
  return 0;
}

#endif /* not H_TAROT_CARDS_PRIVATE_IMPL_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
