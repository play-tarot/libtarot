/**
 * file cards.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/cards_private.h>
#include <stdlib.h>
#include <assert.h>
#include "xalloc.h"

int
tarot_of (TarotNumber n, TarotSuit s, TarotCard * c)
{
  return of (n, s, c);
}

int
tarot_decompose (TarotCard c, TarotNumber * n, TarotSuit * s)
{
  return decompose (c, n, s);
}

TarotCard
tarot_string_to_card (const char *text)
{
  char *end = NULL;
  TarotCard ret = card_parse (text, &end);
  if (end != text && *end == '\0')
    {
      return ret;
    }
  return (TarotCard) (-1);
}

TarotCard
tarot_string_to_card_c (const char *text)
{
  char *end = NULL;
  TarotCard ret = card_parse_c (text, &end);
  if (end != text && *end == '\0')
    {
      return ret;
    }
  return (TarotCard) (-1);
}

TarotCard
tarot_card_parse (const char *text, char **end)
{
  return card_parse (text, end);
}

TarotCard
tarot_card_parse_c (const char *text, char **end)
{
  return card_parse_c (text, end);
}

size_t
tarot_card_to_string (TarotCard c, size_t max, char *dest)
{
  return card_to_string (c, max, dest);
}

char *
tarot_card_to_string_alloc (TarotCard p)
{
  size_t n = card_to_string (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t check = card_to_string (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}

size_t
tarot_card_to_string_c (TarotCard c, size_t max, char *dest)
{
  return card_to_string_c (c, max, dest);
}

char *
tarot_card_to_string_c_alloc (TarotCard p)
{
  size_t n = card_to_string_c (p, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t check = card_to_string_c (p, n + 1, ret);
  assert (check == n);
  assert (ret[n] == '\0');
  return ret;
}

#include <tarot/cards_private_impl.h>
