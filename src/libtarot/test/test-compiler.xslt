<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:tarot="http://planete-kraus.eu/tarot"
		version="1.0">
  <xsl:output method="text" indent="no"/>
  <xsl:strip-space elements="*" />

  <xsl:template match="/">
    <xsl:text>#ifndef HAVE_CONFIG_H&#xA;#include &lt;config.h>&#xA;#endif /* HAVE_CONFIG_H */&#xA;&#xA;#include &lt;tarot.h>&#xA;#include &lt;stdio.h>&#xA;#include &lt;stdlib.h>&#xA;#include &lt;string.h>&#xA;#include &lt;assert.h>&#xA;&#xA;int&#xA;main ()&#xA;{&#xA;  if (tarot_init (LOCALEDIR) != 0)&#xA;    {&#xA;      fprintf (stderr, "Error: could not load libtarot.\n");&#xA;      return EXIT_FAILURE;&#xA;    }&#xA;  {&#xA;    TarotGame *game = tarot_game_alloc ();</xsl:text>
    <xsl:apply-templates select="/tarot:test/tarot:game" />
    <xsl:apply-templates select="/tarot:test/tarot:status" />
    <xsl:text>&#xA;    tarot_game_free (game);&#xA;  }&#xA;  tarot_quit ();&#xA;  return EXIT_SUCCESS;&#xA;}</xsl:text>
  </xsl:template>

  <!-- The following templates are able to load a game called "game",
       so that we can run tests on that game. -->

  <xsl:template match="tarot:game">
    <xsl:text>&#xA;    TarotGameEvent *event = NULL;&#xA;    (void) event;</xsl:text>
    <xsl:apply-templates mode="game" />
  </xsl:template>

  <xsl:template name="add-the-event">
    <xsl:text>&#xA;    if (tarot_game_add_event (game, event) != TAROT_GAME_OK)&#xA;      {&#xA;        fprintf (stderr, "%s:%d: Error: could not add the event.", __FILE__, __LINE__);&#xA;        return EXIT_FAILURE;&#xA;      }&#xA;    tarot_game_event_free (event);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:setup" mode="game">
    <xsl:text>&#xA;    event = tarot_game_event_alloc_setup (</xsl:text>
    <xsl:value-of select="@n-players" />
    <xsl:text>, </xsl:text>
    <xsl:choose>
      <xsl:when test="@with-call = 'yes'">
	<xsl:text>1</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:text>0</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>);</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:deal" mode="game">
    <xsl:text>&#xA;    {&#xA;      const TarotPlayer player = tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@to" />
    <xsl:text>&quot;);&#xA;      TarotCard cards[</xsl:text>
    <xsl:value-of select="count(tarot:card)" />
    <xsl:text>];&#xA;      size_t i = 0;&#xA;      assert (player != ((TarotPlayer) (-1)));</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      cards[i] = tarot_string_to_card_c (&quot;</xsl:text>
      <xsl:value-of select="@card" />
      <xsl:text>&quot;);&#xA;      assert (cards[i] != ((TarotCard) (-1)));&#xA;      i++;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      event = tarot_game_event_alloc_deal (player, sizeof (cards) / sizeof (cards[0]), cards);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:deal-all" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer owners[</xsl:text>
    <xsl:value-of select="count(tarot:player)" />
    <xsl:text>];&#xA;      size_t i = 0;</xsl:text>
    <xsl:for-each select="tarot:player">
      <xsl:text>&#xA;      owners[i] = tarot_string_to_player_c (&quot;</xsl:text>
      <xsl:value-of select="@player" />
      <xsl:text>&quot;);&#xA;      assert (owners[i] != ((TarotPlayer) (-1)));&#xA;      i++;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      event = tarot_game_event_alloc_deal_all (sizeof (owners) / sizeof (owners[0]), owners);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:bid" mode="game">
    <xsl:choose>
      <xsl:when test="@bid = 'pass'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_bid (TAROT_PASS);</xsl:text>
      </xsl:when>
      <xsl:when test="@bid = 'take'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_bid (TAROT_TAKE);</xsl:text>
      </xsl:when>
      <xsl:when test="@bid = 'push'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_bid (TAROT_PUSH);</xsl:text>
      </xsl:when>
      <xsl:when test="@bid = 'straight-keep'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_bid (TAROT_STRAIGHT_KEEP);</xsl:text>
      </xsl:when>
      <xsl:when test="@bid = 'double-keep'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_bid (TAROT_DOUBLE_KEEP);</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:message terminate="yes" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:decl" mode="game">
    <xsl:choose>
      <xsl:when test="@slam = 'yes'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_decl (1);</xsl:text>
      </xsl:when>
      <xsl:when test="@slam = 'no'">
	<xsl:text>&#xA;    event = tarot_game_event_alloc_decl (0);</xsl:text>
      </xsl:when>
      <xsl:otherwise>
	<xsl:message terminate="yes" />
      </xsl:otherwise>
    </xsl:choose>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:call" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotCard call = tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;);&#xA;      assert (call != ((TarotCard) (-1)));&#xA;      event = tarot_game_event_alloc_call (call);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:dog" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotCard cards[</xsl:text>
    <xsl:value-of select="count(tarot:card)" />
    <xsl:text>];&#xA;      size_t i = 0;</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      cards[i] = tarot_string_to_card_c (&quot;</xsl:text>
      <xsl:value-of select="@card" />
      <xsl:text>&quot;);&#xA;      assert (cards[i] != ((TarotCard) (-1)));&#xA;      i++;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      event = tarot_game_event_alloc_dog (sizeof (cards) / sizeof (cards[0]), cards);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:discard" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotCard cards[</xsl:text>
    <xsl:value-of select="count(tarot:card)" />
    <xsl:text>];&#xA;      size_t i = 0;</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      cards[i] = tarot_string_to_card_c (&quot;</xsl:text>
      <xsl:value-of select="@card" />
      <xsl:text>&quot;);&#xA;      assert (cards[i] != ((TarotCard) (-1)));&#xA;      i++;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      event = tarot_game_event_alloc_discard (sizeof (cards) / sizeof (cards[0]), cards);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:handful" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotCard cards[</xsl:text>
    <xsl:value-of select="count(tarot:card)" />
    <xsl:text>];&#xA;      size_t i = 0;</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      cards[i] = tarot_string_to_card_c (&quot;</xsl:text>
      <xsl:value-of select="@card" />
      <xsl:text>&quot;);&#xA;      assert (cards[i] != ((TarotCard) (-1)));&#xA;      i++;</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      event = tarot_game_event_alloc_handful (sizeof (cards) / sizeof (cards[0]), cards);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <xsl:template match="tarot:card" mode="game">
    <xsl:text>&#xA;    {&#xA;      TarotCard card = tarot_string_to_card_c (&quot;&quot;)&#xA;      assert (card != ((TarotCard) (-1)));&#xA;      event = tarot_game_event_alloc_card (card);&#xA;    }</xsl:text>
    <xsl:call-template name="add-the-event" />
  </xsl:template>

  <!-- In the next section, we actually query the game properties. -->

  <xsl:template match="tarot:status">
    <xsl:text>&#xA;    /* Now running the tests */</xsl:text>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'setup']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_SETUP);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'deal']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_DEAL);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'bids']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_BIDS);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'decls']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_DECLS);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'call']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_CALL);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'dog']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_DOG);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'discard']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_DISCARD);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'tricks']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_TRICKS);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:step[@step = 'end']">
    <xsl:text>&#xA;    assert (tarot_game_step (game) == TAROT_END);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:n-players">
    <xsl:text>&#xA;    assert (tarot_game_n_players (game) == </xsl:text>
    <xsl:value-of select="@n" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:with-call[@with-call = 'no']">
    <xsl:text>&#xA;    assert (!tarot_game_with_call (game));</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:with-call[@with-call = 'yes']">
    <xsl:text>&#xA;    assert (tarot_game_with_call (game));</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:n-tricks">
    <xsl:text>&#xA;    assert (tarot_game_n_tricks (game) == </xsl:text>
    <xsl:value-of select="@n" />
    <xsl:text>);</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-next">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      assert (tarot_game_get_next (game, &amp;next) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-next">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      assert (tarot_game_get_next (game, &amp;next) == TAROT_GAME_OK);&#xA;      assert (next != ((TarotPlayer) (-1)));&#xA;      assert (next == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-main-player">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer p;&#xA;      assert (tarot_game_get_main_player (game, &amp;p) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-main-player">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer p;&#xA;      assert (tarot_game_get_main_player (game, &amp;p) == TAROT_GAME_OK);&#xA;      assert (p != ((TarotPlayer) (-1)));&#xA;      assert (p == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-deal-all">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_deal_all (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-deal-all">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      TarotPlayer owner;</xsl:text>
    <xsl:for-each select="tarot:player">
      <xsl:text>&#xA;      assert (tarot_game_get_deal_all (game, &amp;n, i++, 1, &amp;owner) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:player)" /><xsl:text>);&#xA;      assert (owner != ((TarotPlayer) (-1)));&#xA;      assert (owner == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;));</xsl:text>  
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-deal-of">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      TarotPlayer who = tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;);&#xA;      assert (who != ((TarotPlayer) (-1)));&#xA;      assert (tarot_game_get_deal_of (game, who, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-deal-of">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      TarotCard c;&#xA;      TarotPlayer who = tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;);&#xA;      assert (who != ((TarotPlayer) (-1)));</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      assert (tarot_game_get_deal_of (game, who, &amp;n, i++, 1, &amp;c) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:card)" /><xsl:text>);&#xA;      assert (c != ((TarotCard) (-1)));&#xA;      assert (c == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;));</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-cards">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      TarotPlayer who = tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;);&#xA;      assert (who != ((TarotPlayer) (-1)));&#xA;      assert (tarot_game_get_cards (game, who, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-cards">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      TarotCard c;&#xA;      TarotPlayer who = tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;);&#xA;      assert (who != ((TarotPlayer) (-1)));</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      assert (tarot_game_get_cards (game, who, &amp;n, i++, 1, &amp;c) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:card)" /><xsl:text>);&#xA;      assert (c != ((TarotCard) (-1)));&#xA;      assert (c == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;));</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-bids">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_bids (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-bids">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      TarotBid b;</xsl:text>
    <xsl:for-each select="tarot:bid">
      <xsl:text>&#xA;      assert (tarot_game_get_bids (game, &amp;n, i++, 1, &amp;b) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:bid)" /><xsl:text>);</xsl:text>
      <xsl:choose>
	<xsl:when test="@bid = 'pass'">
	  <xsl:text>&#xA;      assert (b == TAROT_PASS);</xsl:text>
	</xsl:when>
	<xsl:when test="@bid = 'take'">
	  <xsl:text>&#xA;      assert (b == TAROT_TAKE);</xsl:text>
	</xsl:when>
	<xsl:when test="@bid = 'push'">
	  <xsl:text>&#xA;      assert (b == TAROT_PUSH);</xsl:text>
	</xsl:when>
	<xsl:when test="@bid = 'straight-keep'">
	  <xsl:text>&#xA;      assert (b == TAROT_STRAIGHT_KEEP);</xsl:text>
	</xsl:when>
	<xsl:when test="@bid = 'double-keep'">
	  <xsl:text>&#xA;      assert (b == TAROT_DOUBLE_KEEP);</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:message terminate="yes" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-taker">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer taker;&#xA;      assert (tarot_game_get_taker (game, &amp;taker) == TAROT_GAME_NA);&#xA;      assert (taker != ((TarotPlayer) (-1)));&#xA;      assert (taker == tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-taker">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer taker;&#xA;      assert (tarot_game_get_taker (game, &amp;taker) == TAROT_GAME_OK);&#xA;      assert (taker != ((TarotPlayer) (-1)));&#xA;      assert (taker == tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-declarations">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_declarations (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-declarations">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      int d;</xsl:text>
    <xsl:for-each select="tarot:decl">
      <xsl:text>&#xA;      assert (tarot_game_get_declarations (game, &amp;n, i++, 1, &amp;d) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:decl)" /><xsl:text>);</xsl:text>
      <xsl:choose>
	<xsl:when test="@decl = 'pass'">
	  <xsl:text>&#xA;      assert (!d);</xsl:text>
	</xsl:when>
	<xsl:when test="@decl = 'slam'">
	  <xsl:text>&#xA;      assert (d);</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:message terminate="yes" />
	</xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-declarant">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer declarant;&#xA;      assert (tarot_game_get_declarant (game, &amp;declarant) == TAROT_GAME_NA);&#xA;      assert (declarant != ((TarotPlayer) (-1)));&#xA;      assert (declarant == tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-declarant">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer declarant;&#xA;      assert (tarot_game_get_declarant (game, &amp;declarant) == TAROT_GAME_OK);&#xA;      assert (declarant != ((TarotPlayer) (-1)));&#xA;      assert (declarant == tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-call">
    <xsl:text>&#xA;    {&#xA;      TarotCard call;&#xA;      assert (tarot_game_get_call (game, &amp;call) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-call">
    <xsl:text>&#xA;    {&#xA;      TarotCard call;&#xA;      assert (tarot_game_get_call (game, &amp;call) == TAROT_GAME_OK);&#xA;      assert (call != (TarotCard) (-1));&#xA;      assert (call == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@call" /><xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-partner">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer partner;&#xA;      assert (tarot_game_get_partner (game, &amp;partner) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-partner">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer partner;&#xA;      assert (tarot_game_get_partner (game, &amp;partner) == TAROT_GAME_OK);&#xA;      assert (partner != (TarotPlayer) (-1));&#xA;      assert (partner == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="@player" /><xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-dog">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_dog (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-dog">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      size_t i = 0;&#xA;      TarotCard c;</xsl:text>
    <xsl:for-each select="tarot:card">
      <xsl:text>&#xA;      assert (tarot_game_get_dog (game, &amp;n, i++, 1, &amp;c) == TAROT_GAME_OK);&#xA;      assert (n == </xsl:text><xsl:value-of select="count(../tarot:card)" /><xsl:text>);&#xA;      assert (c != ((TarotCard) (-1)));&#xA;      assert (c == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;));</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-full-discard">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_full_discard (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-public-discard">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      assert (tarot_game_get_public_discard (game, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-handful">
    <xsl:text>&#xA;    {&#xA;      size_t n;&#xA;      int size;&#xA;      TarotPlayer who = tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@player" />
    <xsl:text>&quot;);&#xA;      assert (who != ((TarotPlayer) (-1)));&#xA;      assert (tarot_game_get_handful (game, who, &amp;size, &amp;n, 0, 0, NULL) == TAROT_GAME_NA);&#xA;      assert (n == 0);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-current-trick">
    <xsl:text>&#xA;    {&#xA;      size_t trick;&#xA;      assert (tarot_game_get_current_trick (game, &amp;trick) == TAROT_GAME_NA);&#xA;      assert (trick == </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-lead-suit">
    <xsl:text>&#xA;    {&#xA;      TarotSuit lead_suit;&#xA;      assert (tarot_game_get_lead_suit (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;lead_suit) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-max-trump">
    <xsl:text>&#xA;    {&#xA;      TarotNumber max_trump;&#xA;      assert (tarot_game_get_max_trump (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;max_trump) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-trick-leader">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer trick_leader;&#xA;      assert (tarot_game_get_trick_leader (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;trick_leader) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-trick-taker">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer trick_taker;&#xA;      assert (tarot_game_get_trick_taker (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;trick_taker) == TAROT_GAME_NA);&#xA;      assert (trick_taker != ((TarotPlayer) (-1)));&#xA;      assert (trick_taker == tarot_string_to_player_c (&quot;</xsl:text>
    <xsl:value-of select="@taker" />
    <xsl:text>&quot;));&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-trick-cards">
    <xsl:text>&#xA;    {&#xA;      size_t n_cards;&#xA;      assert (tarot_game_get_trick_cards (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;n_cards, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-points-in-trick">
    <xsl:text>&#xA;    {&#xA;      unsigned int hp;&#xA;      unsigned int od;&#xA;      assert (tarot_game_get_points_in_trick (game, </xsl:text>
    <xsl:value-of select="@trick" />
    <xsl:text>, &amp;hp, &amp;od) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-scores">
    <xsl:text>&#xA;    {&#xA;      size_t n_players;&#xA;      assert (tarot_game_get_scores (game, &amp;n_players, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-bid">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      TarotBid min;&#xA;      assert (tarot_game_get_hint_bid (game, &amp;next, &amp;min) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-decl">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      int allowed;&#xA;      assert (tarot_game_get_hint_decl (game, &amp;next, &amp;allowed) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-call">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      TarotNumber min;&#xA;      assert (tarot_game_get_hint_call (game, &amp;next, &amp;min) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-discard">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      size_t n, n_prio, n_additional;&#xA;      assert (tarot_game_get_hint_discard (game, &amp;next, &amp;n, &amp;n_prio, 0, 0, NULL, &amp;n_additional, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:has-hint-discard">
    <xsl:text>&#xA;    {&#xA;      size_t n, n_set, n_other_set;&#xA;      size_t i = 0;&#xA;      TarotCard c;&#xA;      TarotPlayer next;</xsl:text>
    <xsl:for-each select="tarot:prio/tarot:card">
      <xsl:text>&#xA;      assert (tarot_game_get_hint_discard (game, &amp;next, &amp;n, &amp;n_set, i++, 1, &amp;c, &amp;n_other_set, 0, 0, NULL) == TAROT_GAME_OK);&#xA;      assert (next != (TarotPlayer) (-1));&#xA;      assert (next == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="../../@next" /><xsl:text>&quot;));&#xA;      assert (n == </xsl:text><xsl:value-of select="../../@n" /><xsl:text>);&#xA;      assert (n_set == </xsl:text><xsl:value-of select="count(../tarot:card)" /><xsl:text>);&#xA;      assert (c != ((TarotCard) (-1)));&#xA;      assert (c == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;));</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;      i = 0;</xsl:text>
    <xsl:for-each select="tarot:additional/tarot:card">
      <xsl:text>&#xA;      assert (tarot_game_get_hint_discard (game, &amp;next, &amp;n, &amp;n_other_set, 0, 0, NULL, &amp;n_set, i++, 1, &amp;c) == TAROT_GAME_OK);&#xA;      assert (next != (TarotPlayer) (-1));&#xA;      assert (next == tarot_string_to_player_c (&quot;</xsl:text><xsl:value-of select="../../@next" /><xsl:text>&quot;));&#xA;      assert (n == </xsl:text><xsl:value-of select="../../@n" /><xsl:text>);&#xA;      assert (n_set == </xsl:text><xsl:value-of select="count(../tarot:card)" /><xsl:text>);&#xA;      assert (c != ((TarotCard) (-1)));&#xA;      assert (c == tarot_string_to_card_c (&quot;</xsl:text><xsl:value-of select="@card" /><xsl:text>&quot;));</xsl:text>
    </xsl:for-each>
    <xsl:text>&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-handful">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      size_t n_simple, n_double, n_triple, n_prio, n_additional;&#xA;      assert (tarot_game_get_hint_handful (game, &amp;next, &amp;n_simple, &amp;n_double, &amp;n_triple, &amp;n_prio, 0, 0, NULL, &amp;n_additional, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="tarot:no-hint-card">
    <xsl:text>&#xA;    {&#xA;      TarotPlayer next;&#xA;      size_t n_playable;&#xA;      assert (tarot_game_get_hint_card (game, &amp;next, &amp;n_playable, 0, 0, NULL) == TAROT_GAME_NA);&#xA;    }</xsl:text>
  </xsl:template>

  <xsl:template match="cannot-autoreveal">
    <xsl:text>&#xA;    {&#xA;      size_t n_dog;&#xA;      assert (!tarot_game_can_autoreveal (game, &amp;n_dog, 0, 0, NULL));&#xA;    }</xsl:text>
  </xsl:template>
</xsl:stylesheet>
