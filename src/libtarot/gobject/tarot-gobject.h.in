/*** BEGIN file-header ***/

/*
 * file tarot-gobject.h.in
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GOBJECT_INCLUDED
#define H_TAROT_GOBJECT_INCLUDED

#include <tarot.h>
#include <glib-2.0/glib-object.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  GType tarot_counter_get_type (void);
  GType tarot_game_get_type (void);
  GType tarot_game_event_get_type (void);
  GType tarot_game_iterator_get_type (void);
  GType tarot_mcts_get_type (void);
  GType tarot_xml_parser_get_type (void);
  GType tarot_xml_parser_iterator_get_type (void);

/* *INDENT-OFF* */  
/*** END file-header ***/
/*** BEGIN file-production ***/
  /* enumerations from "@filename@" */
/*** END file-production ***/
/*** BEGIN value-header ***/
  GType @enum_name@_get_type (void);
#define @ENUMPREFIX@_TYPE_@ENUMSHORT@ (@enum_name@_get_type ())
/*** END value-header ***/
/*** BEGIN file-tail ***/
/* *INDENT-ON* */  
#ifdef __cplusplus
}
#endif                          /* __cplusplus */
#endif                          /* not H_TAROT_GOBJECT_INCLUDED */
/*** END file-tail ***/

/* Local Variables: */
/* mode: c */
/* End: */
