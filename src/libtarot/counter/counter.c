/**
 * file tarot/counter.c Count cards and tell who may own what.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/counter_private.h>
#include <tarot/cards_private.h>
#include <tarot/player_private.h>
#include <stdlib.h>
#include <assert.h>
#include "xalloc.h"

TarotCounter *
tarot_counter_alloc (void)
{
  size_t alignment;
  size_t required = tarot_counter_construct (0, NULL, &alignment);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = tarot_counter_construct (required, mem, &alignment);
  assert (unused == required);
  (void) unused;
  return mem;
}

TarotCounter *
tarot_counter_dup (const TarotCounter * source)
{
  TarotCounter *ret = tarot_counter_alloc ();
  tarot_counter_copy (ret, source);
  return ret;
}

void
tarot_counter_free (TarotCounter * counter)
{
  free (counter);
}

size_t
tarot_counter_construct (size_t max_mem, char *mem, size_t *alignment)
{
  *alignment = alignof (TarotCounter);
  if (max_mem >= sizeof (TarotCounter))
    {
      assert (((size_t) mem) % (*alignment) == 0);
      counter_initialize ((TarotCounter *) mem);
    }
  return sizeof (TarotCounter);
}

void
tarot_counter_copy (TarotCounter * dest, const TarotCounter * source)
{
  counter_copy (dest, source);
}

size_t
tarot_counter_n_cards_remaining (const TarotCounter * counter,
                                 TarotPlayer who)
{
  return counter_n_cards_remaining (counter, who);
}

size_t
tarot_counter_n_cards_doscard (const TarotCounter * counter)
{
  return counter_n_cards_doscard (counter);
}

int
tarot_counter_may_own (const TarotCounter * counter, size_t n_query,
                       const TarotPlayer * who, TarotCard card)
{
  return counter_may_own (counter, n_query, who, card);
}

int
tarot_counter_may_be_in_doscard (const TarotCounter * counter, TarotCard c)
{
  return counter_may_be_in_doscard (counter, c);
}

void
tarot_counter_load_from_game (TarotCounter * counter, const TarotGame * game)
{
  counter_load_from_game (counter, game);
}

TarotImputationError
tarot_counter_and_game_impute (TarotCounter * counter, TarotGame * game,
                               size_t seed_size, const void *seed)
{
  return counter_and_game_impute (counter, game, seed_size, seed);
}

#include <tarot/counter_private_impl.h>
#include <tarot/cards_private_impl.h>
#include <tarot/player_private_impl.h>
#include <tarot/game_event_private_impl.h>
