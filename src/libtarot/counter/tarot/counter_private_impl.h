/**
 * file tarot/counter_private_impl.h Count cards and tell who owns
 * what
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot_private.h>
#include <tarot/cards_private.h>
#include <tarot/game_event_private.h>
#include <tarot/xml.h>
#include <tarot/game_private.h>

#include <assert.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>
#include <nettle/yarrow.h>

static inline size_t
counter_n_cards (size_t n_players)
{
  switch (n_players)
    {
    case 3:
      return 24;
    case 4:
      return 18;
    case 5:
      return 15;
    }
  assert (0);
  return 0;
}

static void
counter_initialize (TarotCounter * counter)
{
  size_t i = 0;
  counter->n_players = 4;
  for (i = 0; i < 5; i++)
    {
      size_t j = 0;
      counter->by_player[i].n_cards_remaining = 18;
      for (j = 0; j < 4; j++)
        {
          counter->by_player[i].max_of[j] = TAROT_KING;
        }
      counter->by_player[i].max_of[4] = 21;
    }
  for (i = 0; i < 78; i++)
    {
      counter->current_owner_of[i] = TAROT_COUNTER_CARD_OWNER_UNKNOWN;
      counter->may_be_discarded[i] = 1;
    }
  counter->taker = (TarotPlayer) (-1);
}

static inline void
counter_copy (TarotCounter * dest, const TarotCounter * source)
{
  size_t i;
  dest->n_players = source->n_players;
  for (i = 0; i < source->n_players; i++)
    {
      size_t j;
      dest->by_player[i].n_cards_remaining =
        source->by_player[i].n_cards_remaining;
      for (j = 0; j < 5; j++)
        {
          dest->by_player[i].max_of[j] = source->by_player[i].max_of[j];
        }
    }
  for (i = 0; i < 78; i++)
    {
      dest->current_owner_of[i] = source->current_owner_of[i];
      dest->may_be_discarded[i] = source->may_be_discarded[i];
    }
  dest->taker = source->taker;
}

static inline void
counter_set_n_players (TarotCounter * counter, size_t n_players)
{
  size_t i;
  assert (n_players <= 5);
  counter->n_players = n_players;
  for (i = 0; i < 5; i++)
    {
      counter->by_player[i].n_cards_remaining = counter_n_cards (n_players);
    }
}

static inline void
counter_deal (TarotCounter * counter, TarotPlayer who, size_t n,
              const TarotCard * buffer)
{
  size_t i = 0;
  assert (counter->by_player[who].n_cards_remaining == n);
  assert (who < counter->n_players);
  for (i = 0; i < n; i++)
    {
      counter->current_owner_of[buffer[i]] = who;
      counter->may_be_discarded[buffer[i]] = 0;
    }
}


static inline void
counter_deal_all (TarotCounter * counter, size_t n, const TarotPlayer * owner)
{
  size_t i = 0;
  assert (n == 78);
  for (i = 0; i < n; i++)
    {
      if (owner[i] < counter->n_players)
        {
          counter->current_owner_of[i] = owner[i];
        }
      else
        {
          counter->current_owner_of[i] = TAROT_COUNTER_CARD_OWNER_DISCARDED;
        }
    }
}

static inline void
counter_call_face (TarotCounter * counter, TarotPlayer taker,
                   TarotNumber face_called)
{
  TarotPlayer other;
  assert (face_called > 10);
  assert (face_called <= TAROT_KING);
  for (other = 0; other < counter->n_players; other++)
    {
      if (other != taker)
        {
          size_t j = 0;
          for (j = 0; j < 4; j++)
            {
              counter->by_player[other].max_of[j] = face_called;
            }
        }
    }
}

static inline void
counter_dog (TarotCounter * counter, TarotPlayer taker, size_t n,
             const TarotCard * cards)
{
  size_t i = 0;
  assert (n <= 6);
  for (i = 0; i < n; i++)
    {
      counter->current_owner_of[cards[i]] = taker;
    }
  counter->by_player[taker].n_cards_remaining += n;
  counter->taker = taker;
}

static inline void
counter_discard (TarotCounter * counter, size_t n_public,
                 const TarotCard * public)
{
  size_t i = 0;
  size_t n_total =
    78 - counter->n_players * counter_n_cards (counter->n_players);
  assert (n_public <= n_total);
  if (n_public == n_total)
    {
      for (i = 0; i < 78; i++)
        {
          counter->may_be_discarded[i] = 0;
        }
    }
  else
    {
      counter->may_be_discarded[13] = 0;
      counter->may_be_discarded[27] = 0;
      counter->may_be_discarded[41] = 0;
      counter->may_be_discarded[55] = 0;
      for (i = TAROT_PETIT; i <= TAROT_EXCUSE; i++)
        {
          counter->may_be_discarded[i] = 0;
        }
    }
  for (i = 0; i < n_public; i++)
    {
      counter->current_owner_of[public[i]] =
        TAROT_COUNTER_CARD_OWNER_DISCARDED;
      counter->may_be_discarded[public[i]] = 1;
    }
  assert (counter->by_player[counter->taker].n_cards_remaining != 0);
  counter->by_player[counter->taker].n_cards_remaining -= n_total;
}

static inline void
counter_handful (TarotCounter * counter, TarotPlayer player, size_t n,
                 const TarotCard * cards)
{
  size_t i = 0;
  for (i = 0; i < n; i++)
    {
      if (counter->current_owner_of[cards[i]] ==
          TAROT_COUNTER_CARD_OWNER_UNKNOWN)
        {
          counter->current_owner_of[cards[i]] = player;
          counter->may_be_discarded[cards[i]] = 0;
        }
    }
}

static inline void
counter_played (TarotCounter * counter, TarotPlayer player, TarotCard played)
{
  size_t ncr = counter->by_player[player].n_cards_remaining - 1;
  counter->by_player[player].n_cards_remaining = ncr;
  counter->current_owner_of[played] = TAROT_COUNTER_CARD_OWNER_PLAYED;
  counter->may_be_discarded[played] = 0;
}

static void counter_cannot_undertrump (TarotCounter * counter,
                                       TarotPlayer player);

static inline void
counter_cannot_follow (TarotCounter * counter, TarotPlayer player,
                       TarotSuit lead_suit)
{
  if (lead_suit == TAROT_TRUMPS)
    {
      counter_cannot_undertrump (counter, player);
    }
  else
    {
      unsigned int i_suit = (unsigned int) lead_suit;
      assert (i_suit < 4);
      counter->by_player[player].max_of[i_suit] = 0;
    }
}

static inline void
counter_cannot_overtrump (TarotCounter * counter, TarotPlayer player,
                          TarotNumber number)
{
  if (counter->by_player[player].max_of[4] >= number)
    {
      counter->by_player[player].max_of[4] = number - 1;
    }
}

static inline void
counter_cannot_undertrump (TarotCounter * counter, TarotPlayer player)
{
  counter->by_player[player].max_of[4] = 0;
}

static inline size_t
counter_n_cards_remaining (const TarotCounter * gunther, TarotPlayer who)
{
  return gunther->by_player[who].n_cards_remaining;
}

static inline size_t
counter_n_cards_doscard (const TarotCounter * counter)
{
  return 78 - counter_n_cards (counter->n_players) * counter->n_players;
}

static inline int
counter_may_own (const TarotCounter * counter, size_t n,
                 const TarotPlayer * who, TarotCard c)
{
  TarotNumber number;
  TarotSuit suit;
  TarotPlayer owner = counter->current_owner_of[c];
  size_t i = 0;
  for (i = 0; i < n; i++)
    {
      if (who[i] >= counter->n_players)
        {
          return 0;
        }
      if (owner == who[i])
        {
          return 1;
        }
      else if (owner == TAROT_COUNTER_CARD_OWNER_UNKNOWN)
        {
          int is_excuse = tarot_decompose (c, &number, &suit);
          unsigned int s_n = (unsigned int) suit;
          if (is_excuse)
            {
              return 1;
            }
          else
            {
              return counter->by_player[who[i]].max_of[s_n] >= number;
            }
        }
      else
        {
          return 0;
        }
    }
  return 0;
}

static inline int
counter_may_be_in_doscard (const TarotCounter * counter, TarotCard c)
{
  return counter->may_be_discarded[c];
}

static inline int
counter_and_game_add_event (TarotCounter * counter, TarotGame * game,
                            const TarotGameEvent * event)
{
  int has_next;
  TarotPlayer next;
  int has_taker;
  TarotPlayer taker;
  size_t n_cards;
  size_t n_players;
  int with_call;
  TarotCard call;
  int is_excuse;
  TarotNumber number;
  TarotSuit suit;
  int cno, cnu, cnf;
  TarotPlayer myself;
  TarotPlayer n;
  TarotCard card;
  if (!game_check_event (game, event))
    {
      return 1;
    }
  has_next = (game_get_next (game, &next) == TAROT_GAME_OK);
  has_taker = (game_get_taker (game, &taker) == TAROT_GAME_OK);
  if (event_get_card (event, &card) == TAROT_EVENT_OK)
    {
      game_check_card (game, card, &cnf, &suit, &cno, &number, &cnu);
    }
  switch (event_type (event))
    {
    case TAROT_SETUP_EVENT:
      if (event_get_setup (event, &n_players, &with_call) != 0)
        {
          assert (0);
        }
      counter_set_n_players (counter, n_players);
      break;
    case TAROT_DEAL_EVENT:
      myself = event->u.deal;
      counter_deal (counter, myself, event->n, event->data);
      break;
    case TAROT_DEAL_ALL_EVENT:
      /* The counter is not accurate on fully-known games, because it
         uses the information of all players, which makes its output
         not generalizable to real-world ai scenarii. */
      assert (event->n == 78);
      n = game_n_players (game);
      for (myself = 0; myself < n; myself++)
        {
          TarotCard cards[78];
          n_cards = 0;
          for (card = 0; card < 78; card++)
            {
              if (event->data[card] == myself)
                {
                  cards[n_cards++] = card;
                }
            }
          counter_deal (counter, myself, n_cards, cards);
        }
      break;
    case TAROT_CALL_EVENT:
      assert (has_taker);
      call = event->u.call;
      is_excuse = tarot_decompose (call, &number, &suit);
      assert (!is_excuse);
      counter_call_face (counter, taker, number);
      break;
    case TAROT_DOG_EVENT:
      assert (has_taker);
      counter_dog (counter, taker, event->n, event->data);
      break;
    case TAROT_DISCARD_EVENT:
      counter_discard (counter, event->n, event->data);
      break;
    case TAROT_HANDFUL_EVENT:
      counter_handful (counter, next, event->n, event->data);
      break;
    case TAROT_CARD_EVENT:
      card = event->u.card;
      assert (has_next);
      counter_played (counter, next, card);
      if (cnf)
        {
          counter_cannot_follow (counter, next, suit);
        }
      if (cno)
        {
          counter_cannot_overtrump (counter, next, number);
        }
      if (cnu)
        {
          counter_cannot_undertrump (counter, next);
        }
      break;
    default:
      break;
    }
  if (game_add_event (game, event) != TAROT_GAME_OK)
    {
      /* Should have been refused earlier */
      assert (0);
    }
  (void) is_excuse;
  return 0;
}

static inline void
counter_load_from_game (TarotCounter * counter, const TarotGame * game)
{
  TarotGame rebuilt;
  TarotGameIterator iterator;
  game_initialize (&rebuilt, 4, 0);
  counter_initialize (counter);
  game_iterator_setup (&iterator, game);
  const TarotGameEvent *event = NULL;
  while ((event = game_iterator_next_value (&iterator)) != NULL)
    {
      if (counter_and_game_add_event (counter, &rebuilt, event) != 0)
        {
          fprintf (stderr,
                   "%s:%d: could not add an event of type %d (game is at step %d)\n",
                   __FILE__, __LINE__, event_type (event),
                   game_step (&rebuilt));
          assert (0);
        }
    }
}

static inline int
counter_assign_missing_card (TarotCounter * counter, TarotCard card,
                             TarotPlayer owner)
{
  if (owner < counter->n_players)
    {
      counter->current_owner_of[card] = owner;
      counter->may_be_discarded[card] = 0;
    }
  else
    {
      counter->current_owner_of[card] = TAROT_COUNTER_CARD_OWNER_DISCARDED;
      assert (counter->may_be_discarded[card]);
    }
  return 0;
}

typedef struct
{
  size_t first;
  size_t second;
} Modification;

typedef struct
{
  TarotCard card;
  TarotPlayer assigned;
} RemainingCard;

typedef struct
{
  size_t n_remaining;
  RemainingCard *cards;
  size_t n_offending;
  size_t *i_offending;
} Solution;

static double compute_energy (const TarotCounter * counter,
                              Solution * solution);
static double update_temperature (double t, unsigned int k);
static void modify (const Solution * solution, struct yarrow256_ctx *prng,
                    double temperature, Modification * modification);
static void apply (Solution * solution, const Modification * modification);
static void undo (Solution * solution, const Modification * modification);
static int accept (double gain, double temperature, double draw);

static int game_is_imputed (const TarotGame * game);

static inline TarotImputationError
counter_and_game_impute (TarotCounter * counter,
                         TarotGame * game, size_t seed_size, const void *seed)
{
  unsigned int k, kmax = 200;
  double energy = 0;
  double temperature = 78;
  struct yarrow256_ctx generator;
  RemainingCard remaining_buffer[78];
  size_t i_offending[78];
  Solution current_solution;
  TarotCard c;
  size_t i, n_players = counter->n_players;
  TarotPlayer p;
  size_t n_to_deal[5];
  assert (n_players <= 5);
  if (game_step (game) == TAROT_END)
    {
      return TAROT_IMPUTATION_OK;
    }
  for (i = 0; i < n_players; i++)
    {
      n_to_deal[i] = counter->by_player[i].n_cards_remaining;
    }
  yarrow256_init (&generator, 0, NULL);
  yarrow256_seed (&generator, seed_size, seed);
  /* Initialization: respect the past tricks, and the number of cards
     per player.  Do not respect who may have what. */
  current_solution.n_remaining = 0;
  current_solution.cards = remaining_buffer;
  current_solution.n_offending = 0;
  current_solution.i_offending = i_offending;
  for (c = 0; c < 78; c++)
    {
      p = counter->current_owner_of[c];
      /* Of course, we only deal the cards for which we don't know the
         owner. */
      if (p == TAROT_COUNTER_CARD_OWNER_UNKNOWN
          || (p < n_players && counter->may_be_discarded[c]))
        {
          current_solution.cards[current_solution.n_remaining++].card = c;
        }
      else if (p < n_players && !(counter->may_be_discarded[c]))
        {
          assert (n_to_deal[p] != 0);
          n_to_deal[p]--;
        }
    }
  i = 0;
  for (p = 0; p < n_players; p++)
    {
      size_t j;
      for (j = 0; j < n_to_deal[p]; j++)
        {
          current_solution.cards[i++].assigned = p;
        }
    }
  for (; i < current_solution.n_remaining; i++)
    {
      current_solution.cards[i].assigned = n_players;
    }
  if (current_solution.n_remaining == 0)
    {
      return TAROT_IMPUTATION_OK;
    }
  /* Shuffle the deal once, otherwise the first player would have more
     hearts. */
  for (i = 1; i < current_solution.n_remaining; i++)
    {
      size_t j = 0;
      Modification modif;
      yarrow256_random (&generator, sizeof (size_t), (unsigned char *) &j);
      j = j % (i + 1);
      modif.first = i;
      modif.second = j;
      apply (&current_solution, &modif);
    }
  for (k = 0; k < kmax; k++)
    {
      double new_energy;
      const double random_max = (double) UINT_MAX + 1.0;
      unsigned int random_value;
      Modification modification;
      modify (&current_solution, &generator, temperature, &modification);
      apply (&current_solution, &modification);
      new_energy = compute_energy (counter, &current_solution);
      yarrow256_random (&generator, sizeof (unsigned int),
                        (unsigned char *) &random_value);
      if (accept
          (new_energy - energy, temperature, random_value / random_max))
        {
          energy = new_energy;
        }
      else
        {
          undo (&current_solution, &modification);
        }
      temperature = update_temperature (temperature, k);
    }
  if (energy < 0.5)
    {
      for (i = 0; i < current_solution.n_remaining; i++)
        {
          c = current_solution.cards[i].card;
          p = current_solution.cards[i].assigned;
          if (game_assign_missing_card (game, c, p) != 0
              || counter_assign_missing_card (counter, c, p) != 0)
            {
              assert (0);
            }
        }
      /* Also add the cards that we know have only one owner. */
      for (c = 0; c < 78; c++)
        {
          p = counter->current_owner_of[c];
          if (p < n_players && !(counter->may_be_discarded[c]))
            {
              if (game_assign_missing_card (game, c, p) != 0
                  || counter_assign_missing_card (counter, c, p) != 0)
                {
                  assert (0);
                }
            }
          else if (p == TAROT_COUNTER_CARD_OWNER_DISCARDED)
            {
              p = n_players;
              if (game_assign_missing_card (game, c, p) != 0
                  || counter_assign_missing_card (counter, c, p) != 0)
                {
                  assert (0);
                }
            }
        }
      assert (game_is_imputed (game));
      (void) &game_is_imputed;
    }
  if (energy >= 0.5)
    {
      return TAROT_IMPUTATION_FAILED;
    }
  return TAROT_IMPUTATION_OK;
}

static int
game_is_imputed (const TarotGame * game)
{
  TarotPlayer player;
  size_t n_players = game_n_players (game);
  int ret = 1;
  for (player = 0; player < n_players; player++)
    {
      size_t n_cards;
      int has_cards =
        (game_get_cards (game, player, &n_cards, 0, 0, NULL) ==
         TAROT_GAME_OK);
      if (game_step (game) > TAROT_DEAL && !has_cards)
        {
          fprintf (stderr, "%s:%d: Player P%u does not have cards.\n",
                   __FILE__, __LINE__, player + 1);
          ret = 0;
        }
    }
  return ret;
}

static double
compute_energy (const TarotCounter * counter, Solution * solution)
{
  size_t i;
  size_t n_players = counter->n_players;
  double ret = 0;
  solution->n_offending = 0;
  for (i = 0; i < solution->n_remaining; i++)
    {
      TarotCard card = solution->cards[i].card;
      TarotPlayer assigned = solution->cards[i].assigned;
      if (assigned < n_players)
        {
          if (counter_may_own (counter, 1, &assigned, card) == 0)
            {
              ret += 1.0;
              solution->i_offending[(solution->n_offending)++] = i;
            }
        }
      else
        {
          if (counter_may_be_in_doscard (counter, card) == 0)
            {
              ret += 1.0;
              solution->i_offending[(solution->n_offending)++] = i;
            }
        }
    }
  return ret;
}

static double
update_temperature (double t, unsigned int k)
{
  /* The initial temperature is 78.  We want to have a decreasing
     exponential, so that in 100 iterations we have our solution.  We
     want to have the last 20 steps cold, i.e. the temperature at 80
     is 1.  So we have an update of exp (-ln (78) / 80) = 0.947 */
  (void) k;
  return t * 0.947;
}

static void
modify (const Solution * solution, struct yarrow256_ctx *prng,
        double temperature, Modification * modification)
{
  size_t random_value;
  if (temperature < 1.0)
    {
      if (solution->n_offending != 0)
        {
          /* If we're cold, we don't even envision modifications that would
             increase the energy. */
          size_t random_index;
          yarrow256_random (prng, sizeof (size_t),
                            (unsigned char *) &random_index);
          yarrow256_random (prng, sizeof (size_t),
                            (unsigned char *) &random_value);
          modification->first =
            solution->i_offending[random_index % solution->n_offending];
          modification->second = random_value % solution->n_remaining;
        }
      else
        {
          modification->first = modification->second = 0;
        }
    }
  else
    {
      yarrow256_random (prng, sizeof (size_t),
                        (unsigned char *) &random_value);
      modification->first = random_value % solution->n_remaining;
      yarrow256_random (prng, sizeof (size_t),
                        (unsigned char *) &random_value);
      modification->second = random_value % solution->n_remaining;
    }
}

static void
apply (Solution * solution, const Modification * modification)
{
  TarotPlayer hold = solution->cards[modification->first].assigned;
  solution->cards[modification->first].assigned =
    solution->cards[modification->second].assigned;
  solution->cards[modification->second].assigned = hold;
}

static void
undo (Solution * solution, const Modification * modification)
{
  apply (solution, modification);
}

static int
accept (double gain, double temperature, double draw)
{
  return (exp (-gain / temperature) > draw);
}

#include <tarot/game_private_impl.h>
