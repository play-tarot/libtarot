/**
 * file tarot/check_hand.c Check that the hand works correctly.
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/hand_private.h>
#include <tarot/cards_private.h>
#include <tarot.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

static inline TarotHand
create_hand (size_t n_max, size_t *n, TarotCard * buffer)
{
  TarotHand ret;
  ret.n_max = n_max;
  ret.n = n;
  ret.buffer = buffer;
  return ret;
}

static const TarotCard set[15] = {
  TAROT_21T, TAROT_20T, TAROT_19T, TAROT_18T, TAROT_13T, TAROT_12T, TAROT_11T,
  TAROT_10T, TAROT_9T, TAROT_8T,
  TAROT_4T, TAROT_1T, TAROT_KH, TAROT_KD, TAROT_QD
};

static const TarotCard added[3] = {
  TAROT_15T, TAROT_KS, TAROT_EXC
};

static const TarotCard removed[3] = {
  TAROT_QD, TAROT_8T, TAROT_4T
};

static const TarotCard expected[15] = {
  TAROT_KH,
  TAROT_KD,
  TAROT_KS,
  TAROT_1T, TAROT_9T, TAROT_10T, TAROT_11T, TAROT_12T, TAROT_13T, TAROT_15T,
  TAROT_18T, TAROT_19T, TAROT_20T, TAROT_21T,
  TAROT_EXC
};

int
main ()
{
  size_t n_set = 15;
  size_t n_added = 3;
  size_t n_removed = 3;
  size_t i = 0;
  TarotCard buffer_set[15];
  TarotCard buffer_added[3];
  TarotCard buffer_removed[3];
  TarotHand h_set = create_hand (15, &n_set, buffer_set);
  TarotHand h_added = create_hand (15, &n_added, buffer_added);
  TarotHand h_removed = create_hand (15, &n_removed, buffer_removed);
  int error = 0;
  error = (hand_set (&h_set, set, n_set)
           || hand_set (&h_added, added, n_added)
           || hand_set (&h_removed, removed, n_removed)
           || hand_swap (&h_set, &h_added, &h_removed));
  if (error != 0)
    {
      abort ();
    }
  if (n_set != 15)
    {
      abort ();
    }
  for (i = 0; i < n_set; i++)
    {
      if (buffer_set[i] != expected[i])
        {
          abort ();
        }
    }
  return 0;
}

#include <tarot/cards_private_impl.h>
#include <tarot/hand_private_impl.h>
