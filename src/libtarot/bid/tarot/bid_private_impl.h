/**
 * file tarot/bid_private_impl.h libtarot code for the management of a
 * bid.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "bid_private.h"
#include <assert.h>

static inline int
bid_discard_allowed (TarotBid bid)
{
  return (bid < TAROT_STRAIGHT_KEEP);
}

static inline int
bid_discard_counted (TarotBid bid)
{
  return (bid < TAROT_DOUBLE_KEEP);
}

static inline int
bid_check (TarotBid base, TarotBid newBid, int *superior)
{
  *superior = 0;
  if (newBid > base)
    {
      *superior = 1;
    }
  return (newBid == TAROT_PASS || newBid > base);
}

static inline int
bid_multiplier (TarotBid bid)
{
  static const int m[5] = {
    0, 1, 2, 4, 6
  };
  unsigned int index = (unsigned int) (bid - TAROT_PASS);
  assert (index < 5);
  return m[index];
}
