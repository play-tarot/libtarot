/*
 * file tarot/bid.h libtarot header for the management of a bid.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_BID_INCLUDED
#define H_TAROT_BID_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  typedef enum
  {
    TAROT_PASS = 0,
    TAROT_TAKE,
    TAROT_PUSH,
    TAROT_STRAIGHT_KEEP,
    TAROT_DOUBLE_KEEP
  } TarotBid;

  /**
   * tarot_bid_discard_allowed:
   * Return: (type boolean):
   */
  int tarot_bid_discard_allowed (TarotBid bid);

  /**
   * tarot_bid_discard_counted:
   * Return: (type boolean):
   */
  int tarot_bid_discard_counted (TarotBid bid);

  /**
   * tarot_bid_check:
   * @superior: (out) (type boolean):
   * Return: (type boolean):
   */
  int tarot_bid_check (TarotBid base, TarotBid new_bid, int *superior);

  /**
   * tarot_bid_multiplier:
   */
  int tarot_bid_multiplier (TarotBid bid);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */
#endif                          /* not H_TAROT_BID_INCLUDED */
