/*
 * file tarot/player.h libtarot header for the management of players.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PLAYER_INCLUDED
#define H_TAROT_PLAYER_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  /**
   * TarotPlayer:
   * A player index.
   */
  typedef unsigned int TarotPlayer;

  /**
   * tarot_string_to_player:
   *
   * Parse a string as a player.  Return ((TarotPlayer) (-1) if it is
   * not valid.
   */
  TarotPlayer tarot_string_to_player (const char *text);

  /**
   * tarot_string_to_player_c:
   *
   * Parse a string as a player, without the locale.  Return
   * ((TarotPlayer) (-1) if it is not valid.
   */
  TarotPlayer tarot_string_to_player_c (const char *text);

  /**
   * tarot_player_parse:
   * end: (transfer none):
   */
  TarotPlayer tarot_player_parse (const char *text, char **end);

  /**
   * tarot_player_parse_c:
   * end: (transfer none):
   */
  TarotPlayer tarot_player_parse_c (const char *text, char **end);

  /**
   * tarot_player_to_string:
   * @dest: (array length=max): where to store the representation
   */
  size_t tarot_player_to_string (TarotPlayer p, size_t max, char *dest);

  /**
   * tarot_player_to_string_alloc: (rename-to tarot_player_to_string)
   */
  char *tarot_player_to_string_alloc (TarotPlayer p);

  /**
   * tarot_player_to_string_c:
   * @dest: (array length=max): where to store the representation
   */
  size_t tarot_player_to_string_c (TarotPlayer p, size_t max, char *dest);

  /**
   * tarot_player_to_string_c_alloc: (rename-to tarot_player_to_string_c)
   */
  char *tarot_player_to_string_c_alloc (TarotPlayer p);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_PLAYER_INCLUDED */
