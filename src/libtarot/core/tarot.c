/**
 * file tarot.c Main libtarot unit.
 *
 * Copyright (C) 2017, 2018, 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "gettext.h"

static int libtarot_initialized = 0;

char *tarot_libtarot_program_name = PACKAGE_NAME;

int
tarot_init (const char *localedir)
{
  assert (libtarot_initialized == 0);
  bindtextdomain (PACKAGE, localedir);
  libtarot_initialized = 1;
  return 0;
}

void
tarot_quit (void)
{
  libtarot_initialized = 0;
}

const char *
tarot_libtarot_version ()
{
  static const char *effective_version = LIBTAROT_VERSION_INFO;
  return effective_version;
}
