/**
 * file tarot_private.h Tarot types definitions
 *
 * Copyright (C) 2017, 2018, 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_PRIVATE_INCLUDED
#define H_TAROT_PRIVATE_INCLUDED

#include <tarot.h>
#include <libxml/parser.h>
#include <stdalign.h>
#include <nettle/yarrow.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  typedef struct
  {
    size_t n_max;
    size_t *n;
    TarotCard *buffer;
  } TarotHand;

  typedef struct
  {
    size_t n;
    TarotCard buffer[3];
  } TarotHand3;

  typedef struct
  {
    size_t n;
    TarotCard buffer[6];
  } TarotHand6;

  typedef struct
  {
    size_t n;
    TarotCard buffer[15];
  } TarotHand15;

  typedef struct
  {
    size_t n;
    TarotCard buffer[18];
  } TarotHand18;

  typedef struct
  {
    size_t n;
    TarotCard buffer[24];
  } TarotHand24;

  typedef struct
  {
    TarotHand24 hands[3];
    int known[3];
  } TarotHands3;

  typedef struct
  {
    TarotHand18 hands[4];
    int known[4];
  } TarotHands4;

  typedef struct
  {
    TarotHand15 hands[5];
    int known[5];
  } TarotHands5;

  typedef struct
  {
    size_t n_players;
    TarotHand hands[5];
    int *known;
  } TarotHandsHandle;

  typedef struct
  {
    size_t n_bids;
    int has_started;
    TarotPlayer taker;
    TarotBid bids[5];
  } TarotBids;

  typedef struct
  {
    size_t n_decls;
    int has_started;
    TarotPlayer slammer;
  } TarotDecls;

  typedef struct
  {
    int has_started;
    int has_card;
    int has_player;
    TarotCard call;
    TarotPlayer partner;
    TarotPlayer taker;
  } TarotCall;

  typedef struct
  {
    int has_started;
    TarotPlayer taker;
    TarotBid contract;
    int dog_done;
    int discard_done;
  } TarotDoscardHeader;

  typedef struct
  {
    TarotDoscardHeader d;
    TarotHand6 dog;
    TarotHand6 discard;
  } TarotDoscard34;

  typedef struct
  {
    TarotDoscardHeader d;
    TarotHand3 dog;
    TarotHand3 discard;
  } TarotDoscard5;

  typedef struct
  {
    size_t n_players;
    TarotDoscardHeader *doscard;
    TarotHand dog;
    TarotHand discard;
  } TarotDoscardHandle;

  typedef struct
  {
    int has_started;
    size_t n_decisions;
    int has_next;
    TarotPlayer leader;
    TarotPlayer shower_of[22];
  } TarotHandfuls;

  typedef struct
  {
    size_t n_cards;
    int last;
    TarotPlayer leader;
    int has_forced_taker;
    TarotPlayer forced_taker;
  } TarotTrickHeader;

  typedef struct
  {
    TarotTrickHeader t;
    TarotCard cards[3];
  } TarotTrick3;

  typedef struct
  {
    TarotTrickHeader t;
    TarotCard cards[4];
  } TarotTrick4;

  typedef struct
  {
    TarotTrickHeader t;
    TarotCard cards[5];
  } TarotTrick5;

  typedef struct
  {
    size_t n_players;
    TarotTrickHeader *trick;
    TarotCard *cards;
  } TarotTrickHandle;

  typedef struct
  {
    size_t i_current;
    TarotTrick3 tricks[24];
  } TarotTricks3;

  typedef struct
  {
    size_t i_current;
    TarotTrick4 tricks[18];
  } TarotTricks4;

  typedef struct
  {
    size_t i_current;
    TarotTrick5 tricks[15];
  } TarotTricks5;

  typedef struct
  {
    size_t n_players;
    size_t n_tricks;
    size_t *i_current;
    TarotTrickHandle tricks[24];
  } TarotTricksHandle;

  typedef struct
  {
    TarotPlayer dealt;
    TarotStep step;
    TarotHands3 hands;
    TarotBids bids;
    TarotDecls decls;
    TarotDoscard34 doscard;
    TarotHandfuls handfuls;
    TarotTricks3 tricks;
  } TarotGame3;

  typedef struct
  {
    TarotPlayer dealt;
    TarotStep step;
    TarotHands4 hands;
    TarotBids bids;
    TarotDecls decls;
    TarotDoscard34 doscard;
    TarotHandfuls handfuls;
    TarotTricks4 tricks;
  } TarotGame4;

  typedef struct
  {
    TarotPlayer dealt;
    TarotStep step;
    TarotHands5 hands;
    TarotBids bids;
    TarotDecls decls;
    TarotDoscard5 doscard;
    TarotHandfuls handfuls;
    TarotTricks5 tricks;
  } TarotGame5;

  typedef struct
  {
    TarotPlayer dealt;
    TarotStep step;
    TarotHands5 hands;
    TarotBids bids;
    TarotDecls decls;
    TarotCall call;
    TarotDoscard5 doscard;
    TarotHandfuls handfuls;
    TarotTricks5 tricks;
  } TarotGame5c;

  typedef struct
  {
    size_t n_players;
    int with_call;
  } TarotGameOptions;

  typedef union
  {
    TarotGame3 p3;
    TarotGame4 p4;
    TarotGame5 p5;
    TarotGame5c p5c;
  } TarotGameU;

  struct TarotGame
  {
    TarotGameOptions opt;
    TarotGameU u;
  };

  typedef union
  {
    TarotGameOptions setup;
    TarotPlayer deal;
    TarotBid bid;
    int decl;
    TarotCard call;
    TarotCard card;
  } TarotGameEventU;

  struct TarotGameEvent
  {
    TarotGameEventT t;
    TarotGameEventU u;
    size_t n;
    unsigned int *data;
  };

  struct TarotGameIterator
  {
    const TarotGame *handle;
    TarotStep step;
    size_t i_player;
    size_t i_trick;
    int handful_done;
    TarotGameEvent last_event;
    unsigned int event_data[78];
  };

  typedef struct
  {
    size_t n_cards_remaining;
    TarotNumber max_of[5];
  } TarotCounterPlayer;

  struct TarotCounter
  {
    size_t n_players;
    TarotCounterPlayer by_player[5];
    TarotPlayer current_owner_of[78];
    int may_be_discarded[78];
    /* Discarded, or still in the unrevealed dog.  A card in the dog
       that may have been discarded is "owned" by the taker, but it
       may be discarded. */
    TarotPlayer taker;
  };

  struct TarotXmlParser
  {
    xmlDocPtr document;
    xmlNodePtr root;
  };

  struct TarotXmlParserIterator
  {
    xmlNodePtr current;
    TarotGameEvent event;
    unsigned int event_data[78];
  };

  struct TarotMctsNode;
  typedef struct TarotMctsNode TarotMctsNode;

  struct TarotMctsNode
  {
    TarotMctsNode *up;
    TarotMctsNode *left;
    TarotMctsNode *right;
    int sum_scores;
    size_t n_simulations;
    TarotPlayer myself;
    TarotGameEvent event;
  };

  struct TarotMcts
  {
    TarotMctsNode *root;
    TarotGame base;
    struct yarrow256_ctx generator;
    double tune_parameter;
    double tune_simulation_random;
    double tune_simulation_agreed;
    char *pool_tail;
    size_t pool_remaining;
    char *pool_head;
  };

#ifdef __cplusplus
}
#endif                          /* __cplusplus */
#endif                          /* not H_TAROT_PRIVATE_INCLUDED */
