/*
 * file mcts.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef H_TAROT_MCTS_INCLUDED
#define H_TAROT_MCTS_INCLUDED
#include <stddef.h>
#include <tarot/player.h>
#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotMcts;
  typedef struct TarotMcts TarotMcts;

  struct TarotGameEvent;
  typedef struct TarotGameEvent TarotGameEvent;

  struct TarotGame;
  typedef struct TarotGame TarotGame;

  /**
   * tarot_mcts_construct:
   * @mem: (array length=max_mem):
   * @alignment: (out):
   */
  size_t tarot_mcts_construct (size_t max_mem, char *mem, size_t *alignment,
                               size_t max_pool);

  void tarot_mcts_copy (TarotMcts * dest, const TarotMcts * source);

  /**
   * tarot_mcts_alloc: (constructor)
   */
  TarotMcts *tarot_mcts_alloc (size_t max_mem);

  TarotMcts *tarot_mcts_dup (size_t new_max_mem, const TarotMcts * source);

  void tarot_mcts_free (TarotMcts * mcts);

  typedef enum
  {
    TAROT_MCTS_OK = 0,
    TAROT_MCTS_NONEXT,
    TAROT_MCTS_UNKNOWNNEXT,
    TAROT_MCTS_NODATA,
    TAROT_MCTS_NOMEM
  } TarotMctsError;

  TarotMctsError tarot_mcts_set_base (TarotMcts * mcts,
                                      const TarotGame * base);

  /**
   * tarot_mcts_seed:
   * @seed: (array length=seed_size) (element-type char):
   */
  void tarot_mcts_seed (TarotMcts * mcts, size_t seed_size, const void *seed);

  /**
   * tarot_mcts_run:
   */
  TarotMctsError tarot_mcts_run (TarotMcts * mcts, size_t n_iterations);

  /**
   * tarot_mcts_set_parameter:
   */
  void tarot_mcts_set_parameter (TarotMcts * mcts, double value);

  /**
   * tarot_mcts_get_parameter:
   */
  double tarot_mcts_get_parameter (const TarotMcts * mcts);

  /**
   * tarot_mcts_set_simulation_random:
   */
  void tarot_mcts_set_simulation_random (TarotMcts * mcts, double weight);

  /**
   * tarot_mcts_get_simulation_random:
   */
  double tarot_mcts_get_simulation_random (const TarotMcts * mcts);

  /**
   * tarot_mcts_set_simulation_agreed:
   */
  void tarot_mcts_set_simulation_agreed (TarotMcts * mcts, double weight);

  /**
   * tarot_mcts_get_simulation_agreed:
   */
  double tarot_mcts_get_simulation_agreed (const TarotMcts * mcts);

  /**
   * tarot_mcts_default_iterations:
   */
  size_t tarot_mcts_default_iterations ();

  /**
   * tarot_mcts_get_best:
   */
  const TarotGameEvent *tarot_mcts_get_best (const TarotMcts * mcts);

  struct TarotMctsNode;
  typedef struct TarotMctsNode TarotMctsNode;
#define TAROT_MCTS_NULL NULL

  const TarotMctsNode *tarot_mcts_root (const TarotMcts * mcts);
  const TarotGameEvent *tarot_mcts_get (const TarotMctsNode * node,
                                        size_t *n_simulations,
                                        int *sum_scores);
  const TarotMctsNode *tarot_mcts_parent (const TarotMctsNode * node);
  const TarotMctsNode *tarot_mcts_first_child (const TarotMctsNode * node);
  const TarotMctsNode *tarot_mcts_next_sibling (const TarotMctsNode * node);
  const TarotMctsNode *tarot_mcts_previous_sibling
    (const TarotMctsNode * node);

  /**
   * tarot_mcts_save_to_xml:
   * @dest: (array length=max):
   */
  size_t tarot_mcts_save_to_xml (const TarotMcts * mcts, size_t start,
                                 size_t max, char *dest);

  char *tarot_mcts_save_to_xml_alloc (const TarotMcts * mcts);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_MCTS_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
