/**
 * file mcts.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/xml.h>
#include <tarot/mcts_private.h>
#include "xalloc.h"
#include <string.h>

size_t
tarot_mcts_construct (size_t max_mem, char *mem, size_t *alignment,
                      size_t max_pool)
{
  return mcts_construct (max_mem, mem, alignment, max_pool);
}

TarotMcts *
tarot_mcts_alloc (size_t max)
{
  size_t alignment;
  size_t required = mcts_construct (0, NULL, &alignment, max);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = mcts_construct (required, mem, &alignment, max);
  assert (unused == required);
  (void) unused;
  return mem;
}

void
tarot_mcts_copy (TarotMcts * dest, const TarotMcts * source)
{
  mcts_copy (dest, source);
}

TarotMcts *
tarot_mcts_dup (size_t new_max_mem, const TarotMcts * source)
{
  TarotMcts *ret = tarot_mcts_alloc (new_max_mem);
  mcts_copy (ret, source);
  return ret;
}

void
tarot_mcts_free (TarotMcts * mcts)
{
  free (mcts);
}

TarotMctsError
tarot_mcts_set_base (TarotMcts * mcts, const TarotGame * base)
{
  return mcts_set_base (mcts, base);
}

void
tarot_mcts_seed (TarotMcts * mcts, size_t seed_size, const void *seed)
{
  return mcts_seed (mcts, seed_size, seed);
}

TarotMctsError
tarot_mcts_run (TarotMcts * mcts, size_t n_iterations)
{
  return mcts_run (mcts, n_iterations);
}

void
tarot_mcts_set_parameter (TarotMcts * mcts, double value)
{
  mcts_set_parameter (mcts, value);
}

double
tarot_mcts_get_parameter (const TarotMcts * mcts)
{
  return mcts_get_parameter (mcts);
}

void
tarot_mcts_set_simulation_random (TarotMcts * mcts, double value)
{
  mcts_set_simulation_random (mcts, value);
}

double
tarot_mcts_get_simulation_random (const TarotMcts * mcts)
{
  return mcts_get_simulation_random (mcts);
}

void
tarot_mcts_set_simulation_agreed (TarotMcts * mcts, double value)
{
  mcts_set_simulation_agreed (mcts, value);
}

double
tarot_mcts_get_simulation_agreed (const TarotMcts * mcts)
{
  return mcts_get_simulation_agreed (mcts);
}

size_t
tarot_mcts_default_iterations ()
{
  return mcts_default_iterations ();
}

const TarotGameEvent *
tarot_mcts_get_best (const TarotMcts * mcts)
{
  return mcts_get_best (mcts);
}

const TarotMctsNode *
tarot_mcts_root (const TarotMcts * mcts)
{
  return mcts_root ((TarotMcts *) mcts);
}

const TarotGameEvent *
tarot_mcts_get (const TarotMctsNode * node, size_t *n_simulations,
                int *sum_scores)
{
  return mcts_get (node, n_simulations, sum_scores);
}

const TarotMctsNode *
tarot_mcts_parent (const TarotMctsNode * node)
{
  return mcts_parent ((TarotMctsNode *) node);
}

const TarotMctsNode *
tarot_mcts_first_child (const TarotMctsNode * node)
{
  return mcts_first_child ((TarotMctsNode *) node);
}

const TarotMctsNode *
tarot_mcts_next_sibling (const TarotMctsNode * node)
{
  return mcts_next_sibling ((TarotMctsNode *) node);
}

const TarotMctsNode *
tarot_mcts_previous_sibling (const TarotMctsNode * node)
{
  return mcts_previous_sibling ((TarotMctsNode *) node);
}

size_t
tarot_mcts_save_to_xml (const TarotMcts * mcts, size_t start, size_t max,
                        char *dest)
{
  return tarot_xml_save_mcts_tree (mcts, start, max, dest);
}

char *
tarot_mcts_save_to_xml_alloc (const TarotMcts * mcts)
{
  size_t n = tarot_mcts_save_to_xml (mcts, 0, 0, NULL);
  char *ret = xmalloc (n + 1);
  size_t n2 = tarot_mcts_save_to_xml (mcts, 0, n + 1, ret);
  assert (n2 == n);
  (void) n2;
  assert (strlen (ret) == n);
  return ret;
}

#include <tarot/mcts_private_impl.h>
