/*
 * file tarot/xml.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_XML_INCLUDED
#define H_TAROT_XML_INCLUDED

#include <tarot/game_event.h>
#include <tarot/mcts.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotXmlParser;
  typedef struct TarotXmlParser TarotXmlParser;

  struct TarotGame;
  typedef struct TarotGame TarotGame;

  /**
   * tarot_xml_parser_construct:
   * @mem: (array length=max_mem):
   * @alignment: (out):
   */
  size_t tarot_xml_parser_construct (size_t max_mem, char *mem,
                                     size_t *alignment, const char *input);
  void tarot_xml_parser_destruct (TarotXmlParser * parser);

  /**
   * tarot_xml_parser_alloc: (constructor)
   */
  TarotXmlParser *tarot_xml_parser_alloc (const char *input);
  TarotXmlParser *tarot_xml_parser_dup (const TarotXmlParser * source);
  void tarot_xml_parser_free (TarotXmlParser * xml_parser);

  struct TarotXmlParserIterator;
  typedef struct TarotXmlParserIterator TarotXmlParserIterator;

  /**
   * tarot_xml_parser_iterator_construct:
   * @mem: (array length=max_mem):
   * @alignment: (out):
   */
  size_t tarot_xml_parser_iterator_construct (size_t max_mem, char *mem,
                                              size_t *alignment,
                                              const TarotXmlParser * parser);
  void tarot_xml_parser_iterator_destruct (TarotXmlParserIterator * parser);

  const TarotGameEvent *tarot_xml_parser_iterator_next_value
    (TarotXmlParserIterator * iterator);
  size_t tarot_xml_save (const TarotGame * game, size_t start, size_t max,
                         char *dest);

  TarotXmlParserIterator *tarot_xml_parser_iterator
    (const TarotXmlParser * parser);
  TarotXmlParserIterator *tarot_xml_parser_iterator_dup
    (const TarotXmlParserIterator * source);
  void tarot_xml_parser_iterator_free (TarotXmlParserIterator * xml_parser);

  /**
   * tarot_xml_save_mcts_tree:
   * @dest: (array length=max):
   */
  size_t tarot_xml_save_mcts_tree (const TarotMcts * mcts, size_t start,
                                   size_t max, char *dest);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_XML_INCLUDED */
