/**
 * file xml.c
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/xml_private.h>
#include <tarot/xml_private_impl.h>
#include <tarot/game_private.h>
#include <tarot/mcts_private.h>
#include "xalloc.h"

size_t
tarot_xml_parser_construct (size_t max_mem, char *mem, size_t *alignment,
                            const char *input)
{
  *alignment = alignof (TarotXmlParser);
  if (max_mem >= sizeof (TarotXmlParser))
    {
      assert (((size_t) mem) % (*alignment) == 0);
      if (xml_construct ((TarotXmlParser *) mem, input) == 0)
        {
          return sizeof (TarotXmlParser);
        }
      return 0;
    }
  return sizeof (TarotXmlParser);
}

TarotXmlParser *
tarot_xml_parser_alloc (const char *input)
{
  size_t alignment;
  size_t required = tarot_xml_parser_construct (0, NULL, &alignment, input);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused = tarot_xml_parser_construct (required, mem, &alignment, input);
  assert (unused == required);
  (void) unused;
  return mem;
}

TarotXmlParser *
tarot_xml_parser_dup (const TarotXmlParser * parser)
{
  /* Not implemented yet */
  (void) parser;
  return NULL;
}

void
tarot_xml_parser_destruct (TarotXmlParser * parser)
{
  xml_destruct (parser);
}

void
tarot_xml_parser_free (TarotXmlParser * parser)
{
  tarot_xml_parser_destruct (parser);
  free (parser);
}

size_t
tarot_xml_parser_iterator_construct (size_t max_mem, char *mem,
                                     size_t *alignment,
                                     const TarotXmlParser * parser)
{
  *alignment = alignof (TarotXmlParserIterator);
  if (max_mem >= sizeof (TarotXmlParserIterator))
    {
      assert (((size_t) mem) % (*alignment) == 0);
      if (xml_iterator_construct ((TarotXmlParserIterator *) mem, parser)
          == 0)
        {
          return sizeof (TarotXmlParserIterator);
        }
      return 0;
    }
  return sizeof (TarotXmlParserIterator);
}

TarotXmlParserIterator *
tarot_xml_parser_iterator (const TarotXmlParser * parser)
{
  size_t alignment;
  size_t required =
    tarot_xml_parser_iterator_construct (0, NULL, &alignment, parser);
  size_t unused;
  void *mem = xmalloc (required);
  assert (alignment >= 1 && alignment <= alignof (max_align_t));
  unused =
    tarot_xml_parser_iterator_construct (required, mem, &alignment, parser);
  assert (unused == required);
  (void) unused;
  return mem;
}

TarotXmlParserIterator *
tarot_xml_parser_iterator_dup (const TarotXmlParserIterator * iterator)
{
  /* Not implemented yet */
  (void) iterator;
  return NULL;
}

void
tarot_xml_parser_iterator_free (TarotXmlParserIterator * iterator)
{
  free (iterator);
}

const TarotGameEvent *
tarot_xml_parser_iterator_next_value (TarotXmlParserIterator * iterator)
{
  if (iterator->current == NULL)
    {
      return NULL;
    }
  while (setup_contents (iterator) != 0)
    {
      iterator->current = iterator->current->next;
      if (iterator->current == NULL)
        {
          return NULL;
        }
    }
  iterator->current = iterator->current->next;
  return &(iterator->event);
}

static int
print_prop_number (xmlNodePtr node, const char *name, size_t value, int sign)
{
  size_t reversed_digits = 0;
  size_t n_digits = 0;
  while (value != 0)
    {
      reversed_digits *= 10;
      reversed_digits += (value % 10);
      value /= 10;
      n_digits++;
    }
  if (n_digits == 0)
    {
      return (xmlNewProp
              (node, (const xmlChar *) name, (const xmlChar *) "0") == NULL);
    }
  else
    {
      char *str = malloc (n_digits + 2);
      size_t i_digit;
      size_t sign_length = 0;
      if (str == NULL)
        {
          return 1;
        }
      if (sign < 0)
        {
          str[0] = '-';
          sign_length = 1;
        }
      for (i_digit = 0; i_digit < n_digits; i_digit++)
        {
          str[i_digit + sign_length] = '0' + (reversed_digits % 10);
          reversed_digits /= 10;
        }
      str[i_digit] = '\0';
      assert (reversed_digits == 0);
      if (xmlNewProp (node, (const xmlChar *) name, (const xmlChar *) str)
          == NULL)
        {
          free (str);
          return 1;
        }
      free (str);
      return 0;
    }
}

static int
print_prop_boolean (xmlNodePtr node, const char *name, int value)
{
  if (value)
    {
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "yes") == NULL);
    }
  return (xmlNewProp (node, (const xmlChar *) name, (const xmlChar *) "no")
          == NULL);
}

static int
print_prop_player (xmlNodePtr node, const char *name, TarotPlayer value)
{
  char *str = NULL;
  size_t needed = tarot_player_to_string_c (value, 0, NULL);
  str = malloc (needed + 1);
  if (str == NULL)
    {
      return 1;
    }
  if (tarot_player_to_string_c (value, needed + 1, str) != needed)
    {
      assert (0);
    }
  if (xmlNewProp (node, (const xmlChar *) name, (const xmlChar *) str) ==
      NULL)
    {
      free (str);
      return 1;
    }
  free (str);
  return 0;
}

static int
print_prop_card (xmlNodePtr node, const char *name, TarotCard value)
{
  char *str = NULL;
  size_t needed = tarot_card_to_string_c (value, 0, NULL);
  str = malloc (needed + 1);
  if (str == NULL)
    {
      return 1;
    }
  if (tarot_card_to_string_c (value, needed + 1, str) != needed)
    {
      assert (0);
    }
  if (xmlNewProp (node, (const xmlChar *) name, (const xmlChar *) str) ==
      NULL)
    {
      free (str);
      return 1;
    }
  free (str);
  return 0;
}

static int
print_prop_bid (xmlNodePtr node, const char *name, TarotBid value)
{
  switch (value)
    {
    case TAROT_PASS:
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "pass") == NULL);
    case TAROT_TAKE:
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "take") == NULL);
    case TAROT_PUSH:
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "push") == NULL);
    case TAROT_STRAIGHT_KEEP:
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "straight-keep") == NULL);
    case TAROT_DOUBLE_KEEP:
      return (xmlNewProp
              (node, (const xmlChar *) name,
               (const xmlChar *) "double-keep") == NULL);
    default:
      break;
    }
  return 1;
}

static xmlNodePtr
print_card (TarotCard card)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) "card");
  if (ret == NULL || print_prop_card (ret, "card", card) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_player (TarotPlayer player)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) "player");
  if (ret == NULL || print_prop_player (ret, "player", player) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_cards (size_t n_cards, const TarotCard * cards, const char *name,
             xmlNsPtr namespace)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) name), card;
  size_t i;
  if (ret == NULL)
    {
      return NULL;
    }
  for (i = 0; i < n_cards; i++)
    {
      card = print_card (cards[i]);
      if (card == NULL || xmlAddChild (ret, card) == NULL)
        {
          xmlFreeNode (card);
          xmlFreeNode (ret);
          return NULL;
        }
      xmlSetNs (card, namespace);
    }
  return ret;
}

static xmlNodePtr
print_players (size_t n_players, const TarotPlayer * players,
               const char *name, xmlNsPtr namespace)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) name), player;
  size_t i;
  if (ret == NULL)
    {
      return NULL;
    }
  for (i = 0; i < n_players; i++)
    {
      player = print_player (players[i]);
      if (player == NULL || xmlAddChild (ret, player) == NULL)
        {
          xmlFreeNode (player);
          xmlFreeNode (ret);
          return NULL;
        }
      xmlSetNs (player, namespace);
    }
  return ret;
}

static xmlNodePtr
print_setup_event (const TarotGameEvent * event)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) "setup");
  size_t n_players;
  int with_call;
  if (event_get_setup (event, &n_players, &with_call) != 0)
    {
      assert (0);
    }
  if (print_prop_number (ret, "n-players", n_players, 0)
      || print_prop_boolean (ret, "with-call", with_call))
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_deal_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  TarotPlayer to = event->u.deal;
  xmlNodePtr ret = NULL;
  size_t n_cards;
  const TarotCard *cards;
  n_cards = event->n;
  cards = event->data;
  ret = print_cards (n_cards, cards, "deal", namespace);
  if (ret == NULL)
    {
      return NULL;
    }
  if (print_prop_player (ret, "to", to) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_deal_all_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  size_t n_players = event->n;
  const TarotPlayer *players = event->data;
  return print_players (n_players, players, "deal-all", namespace);
}

static xmlNodePtr
print_bid_event (const TarotGameEvent * event)
{
  xmlNodePtr ret = NULL;
  TarotBid bid = event->u.bid;
  ret = xmlNewNode (NULL, (const xmlChar *) "bid");
  if (ret == NULL)
    {
      return NULL;
    }
  if (print_prop_bid (ret, "bid", bid) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_decl_event (const TarotGameEvent * event)
{
  xmlNodePtr ret = NULL;
  int decl = event->u.decl;
  ret = xmlNewNode (NULL, (const xmlChar *) "decl");
  if (ret == NULL)
    {
      return NULL;
    }
  if (print_prop_boolean (ret, "slam", decl) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_call_event (const TarotGameEvent * event)
{
  xmlNodePtr ret = NULL;
  TarotCard call = event->u.call;
  ret = xmlNewNode (NULL, (const xmlChar *) "call");
  if (ret == NULL)
    {
      return NULL;
    }
  if (print_prop_card (ret, "card", call) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_dog_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  size_t n_cards = event->n;
  const TarotCard *cards = (TarotCard *) event->data;
  return print_cards (n_cards, cards, "dog", namespace);
}

static xmlNodePtr
print_discard_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  size_t n_cards = event->n;
  const TarotCard *cards = (TarotCard *) event->data;
  return print_cards (n_cards, cards, "discard", namespace);
}

static xmlNodePtr
print_handful_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  size_t n_cards = event->n;
  const TarotCard *cards = (TarotCard *) event->data;
  return print_cards (n_cards, cards, "handful", namespace);
}

static xmlNodePtr
print_card_event (const TarotGameEvent * event)
{
  xmlNodePtr ret = NULL;
  TarotCard card = event->u.card;
  ret = xmlNewNode (NULL, (const xmlChar *) "card");
  if (ret == NULL)
    {
      return NULL;
    }
  if (print_prop_card (ret, "card", card) != 0)
    {
      xmlFreeNode (ret);
      ret = NULL;
    }
  return ret;
}

static xmlNodePtr
print_event (const TarotGameEvent * event, xmlNsPtr namespace)
{
  xmlNodePtr ret = NULL;
  switch (event_type (event))
    {
    case TAROT_SETUP_EVENT:
      ret = print_setup_event (event);
      break;
    case TAROT_DEAL_EVENT:
      ret = print_deal_event (event, namespace);
      break;
    case TAROT_DEAL_ALL_EVENT:
      ret = print_deal_all_event (event, namespace);
      break;
    case TAROT_BID_EVENT:
      ret = print_bid_event (event);
      break;
    case TAROT_DECL_EVENT:
      ret = print_decl_event (event);
      break;
    case TAROT_CALL_EVENT:
      ret = print_call_event (event);
      break;
    case TAROT_DOG_EVENT:
      ret = print_dog_event (event, namespace);
      break;
    case TAROT_DISCARD_EVENT:
      ret = print_discard_event (event, namespace);
      break;
    case TAROT_HANDFUL_EVENT:
      ret = print_handful_event (event, namespace);
      break;
    case TAROT_CARD_EVENT:
      ret = print_card_event (event);
      break;
    default:
      break;
    }
  if (ret != NULL)
    {
      xmlSetNs (ret, namespace);
    }
  return ret;
}

static inline xmlNodePtr
print_events (TarotGameIterator * iterator)
{
  xmlNodePtr root = xmlNewNode (NULL, (const xmlChar *) "game");
  xmlNodePtr event = NULL;
  xmlNsPtr namespace;
  const TarotGameEvent *event_source;
  if (root == NULL)
    {
      return NULL;
    }
  namespace =
    xmlNewNs (root, (const xmlChar *) "http://planete-kraus.eu/tarot",
              (const xmlChar *) "tarot");
  if (namespace == NULL)
    {
      xmlFreeNode (root);
      return NULL;
    }
  while ((event_source = tarot_game_iterator_next_value (iterator)) != NULL)
    {
      event = print_event (event_source, namespace);
      if (event == NULL || xmlAddChild (root, event) == NULL)
        {
          xmlFreeNode (event);
          xmlFreeNode (root);
          return NULL;
        }
      xmlSetNs (event, namespace);
    }
  xmlSetNs (root, namespace);
  return root;
}

static char *
xml_save_alloc (TarotGameIterator * iterator)
{
  xmlDocPtr document = xmlNewDoc ((const xmlChar *) "1.0");
  xmlNodePtr root;
  char *ret;
  xmlChar *xmlbuff;
  int xmlsize;
  root = print_events (iterator);
  if (root == NULL)
    {
      xmlFreeDoc (document);
      return NULL;
    }
  xmlDocSetRootElement (document, root);
  xmlDocDumpFormatMemory (document, &xmlbuff, &xmlsize, 1);
  xmlFreeDoc (document);
  if (xmlbuff == NULL)
    {
      return NULL;
    }
  ret = malloc (xmlsize + 1);
  if (ret != NULL)
    {
      strncpy (ret, (char *) xmlbuff, xmlsize);
      ret[xmlsize] = '\0';
    }
  xmlFree (xmlbuff);
  return ret;
}

size_t
tarot_xml_save (const TarotGame * game, size_t start, size_t max, char *dest)
{
  TarotGameIterator iterator;
  char *data;
  size_t len;
  size_t i;
  game_iterator_setup (&iterator, game);
  data = xml_save_alloc (&iterator);
  len = strlen (data);
  if (max > 0)
    {
      dest[0] = '\0';
    }
  for (i = 0; i < max; i++)
    {
      size_t i_src = start + i;
      if (i_src < len)
        {
          dest[i] = data[i_src];
          if (i + 1 < max)
            {
              dest[i + 1] = '\0';
            }
        }
    }
  free (data);
  return len;
}

static xmlNodePtr print_mcts_node (const TarotMctsNode * node,
                                   xmlNsPtr namespace);

static int
print_mcts_siblings (const TarotMctsNode * node,
                     xmlNodePtr parent, xmlNsPtr namespace)
{
  const TarotMctsNode *sibling = node;
  for (sibling = node;
       sibling != NULL;
       sibling = mcts_next_sibling ((TarotMctsNode *) sibling))
    {
      xmlNodePtr xml_sibling = print_mcts_node (sibling, namespace);
      if (xml_sibling == NULL)
        {
          return 1;
        }
      if (xmlAddChild (parent, xml_sibling) == NULL)
        {
          xmlFreeNode (xml_sibling);
          return 1;
        }
    }
  return 0;
}

static xmlNodePtr
print_mcts_node (const TarotMctsNode * node, xmlNsPtr namespace)
{
  xmlNodePtr ret = xmlNewNode (NULL, (const xmlChar *) "node");
  xmlNodePtr stats = xmlNewNode (NULL, (const xmlChar *) "stats");
  xmlNodePtr event;
  size_t n_simulations;
  int sum_scores;
  const TarotGameEvent *e = mcts_get (node, &n_simulations, &sum_scores);
  if (ret == NULL || stats == NULL || xmlAddChild (ret, stats) == NULL)
    {
      fprintf (stderr, "%s:%d: could not allocate a node\n",
               __FILE__, __LINE__);
      xmlFreeNode (ret);
      return NULL;
    }
  if (print_prop_number (stats, "n-simulations", n_simulations, 0) != 0
      || print_prop_number (stats, "sum-scores",
                            (sum_scores < 0 ? -sum_scores : sum_scores),
                            (sum_scores < 0 ? -1 : 0)) != 0)
    {
      fprintf (stderr, "%s:%d: could not add the stats\n",
               __FILE__, __LINE__);
      xmlFreeNode (ret);
      return NULL;
    }
  event = print_event (e, namespace);
  if (event == NULL || xmlAddChild (stats, event) == NULL)
    {
      fprintf (stderr, "%s:%d: could not copy the stats\n",
               __FILE__, __LINE__);
      xmlFreeNode (event);
      xmlFreeNode (ret);
      return NULL;
    }
  node = mcts_first_child ((TarotMctsNode *) node);
  if (node != NULL)
    {
      if (print_mcts_siblings (node, ret, namespace) != 0)
        {
          fprintf (stderr, "%s:%d: could not add the child\n",
                   __FILE__, __LINE__);
          xmlFreeNode (ret);
          return NULL;
        }
    }
  xmlSetNs (stats, namespace);
  xmlSetNs (ret, namespace);
  return ret;
}

static char *
xml_save_mcts_tree_alloc (const TarotMcts * mcts)
{
  xmlDocPtr document = xmlNewDoc ((const xmlChar *) "1.0");
  xmlNodePtr root;
  char *ret;
  xmlChar *xmlbuff;
  int xmlsize;
  xmlNsPtr namespace;
  root = xmlNewNode (NULL, (const xmlChar *) "tree");
  if (root == NULL)
    {
      xmlFreeDoc (document);
      return NULL;
    }
  namespace =
    xmlNewNs (root, (const xmlChar *) "http://planete-kraus.eu/tarot",
              (const xmlChar *) "tarot");
  if (namespace == NULL)
    {
      xmlFreeDoc (document);
      return NULL;
    }
  if (print_mcts_siblings (mcts_root ((TarotMcts *) mcts), root, namespace)
      != 0)
    {
      xmlFreeNode (root);
      xmlFreeDoc (document);
      return NULL;
    }
  xmlDocSetRootElement (document, root);
  xmlSetNs (root, namespace);
  xmlDocDumpFormatMemory (document, &xmlbuff, &xmlsize, 1);
  xmlFreeDoc (document);
  if (xmlbuff == NULL)
    {
      return NULL;
    }
  ret = malloc (xmlsize + 1);
  if (ret != NULL)
    {
      strncpy (ret, (char *) xmlbuff, xmlsize);
      ret[xmlsize] = '\0';
    }
  xmlFree (xmlbuff);
  return ret;
}

size_t
tarot_xml_save_mcts_tree (const TarotMcts * mcts, size_t start, size_t max,
                          char *dest)
{
  char *data;
  size_t len;
  size_t i;
  data = xml_save_mcts_tree_alloc (mcts);
  len = strlen (data);
  if (max > 0)
    {
      dest[0] = '\0';
    }
  for (i = 0; i < max; i++)
    {
      size_t i_src = start + i;
      if (i_src < len)
        {
          dest[i] = data[i_src];
          if (i + 1 < max)
            {
              dest[i + 1] = '\0';
            }
        }
    }
  free (data);
  return len;
}

#include <tarot/game_private_impl.h>
#include <tarot/mcts_private_impl.h>
