/**
 * file simulation.c
 *
 * Copyright (C) 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot_private.h>
#include <tarot/simulation.h>
#include <nettle/yarrow.h>
#include <tarot/game.h>
#include <assert.h>
#include <stdlib.h>
#include <tarot/cards_private.h>
#include <tarot/game_event_private.h>
#include <tarot/game_private.h>
#include <pool.h>
#include <tuneconfig.h>

static void
select_next_move_deal (size_t n_players, TarotGameEvent * event,
                       struct yarrow256_ctx *rng)
{
  char deal_seed[16];
  size_t sz = sizeof (deal_seed);
  yarrow256_random (rng, sz, (void *) deal_seed);
  event_set_deal_all_random (event, n_players, sz, (void *) deal_seed);
}

static void
select_next_move_bid_1 (TarotGameEvent * event, TarotBid mini,
                        struct yarrow256_ctx *rng)
{
  unsigned char random_byte;
  double random_value;
  yarrow256_random (rng, 1, &random_byte);
  random_value = random_byte / 256.0;
  if (random_value <= 0.5)
    {
      event_set_bid (event, TAROT_PASS);
    }
  else
    {
      /* Outbid */
      event_set_bid (event, mini);
    }
}

static void
select_next_move_bid_2 (TarotGameEvent * event, TarotBid mini,
                        struct yarrow256_ctx *rng)
{
  unsigned char levels[] = {
    192,                        /* Under this, pass */
    224,                        /* Under this, take */
    240,                        /* Under this, push */
    248                         /* Under this, keep (straight) */
  };
  TarotBid bids[] = {
    TAROT_PASS,
    TAROT_TAKE,
    TAROT_PUSH,
    TAROT_STRAIGHT_KEEP,
    TAROT_DOUBLE_KEEP
  };
  size_t n_levels = sizeof (levels) / sizeof (levels[0]);
  size_t n_bids = sizeof (bids) / sizeof (bids[0]);
  size_t i_bid;
  unsigned char random_byte;
  assert (n_levels + 1 == n_bids);
  yarrow256_random (rng, 1, &random_byte);
  for (i_bid = 0; i_bid < n_bids && random_byte > levels[i_bid]; i_bid++)
    ;
  if (i_bid == n_bids)
    {
      i_bid--;
    }
  if (bids[i_bid] < mini)
    {
      event_set_bid (event, TAROT_PASS);
    }
  else
    {
      event_set_bid (event, bids[i_bid]);
    }
  (void) &select_next_move_bid_1;
}

static void
select_next_move_bid (TarotGameEvent * event, TarotBid mini,
                      struct yarrow256_ctx *rng)
{
  if (0)
    {
      select_next_move_bid_1 (event, mini, rng);
    }
  else
    {
      select_next_move_bid_2 (event, mini, rng);
    }
}

static void
select_next_move_decl (TarotGameEvent * event, int allowed,
                       struct yarrow256_ctx *rng)
{
  (void) allowed;
  (void) rng;
  event_set_decl (event, 0);
}

static void
select_next_move_call (TarotGameEvent * event, TarotNumber mini_face,
                       struct yarrow256_ctx *rng)
{
  TarotNumber face;
  static TarotSuit suits[4] =
    { TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES };
  TarotSuit suit;
  TarotCard call;
  unsigned int random_value;
  if (mini_face == TAROT_KING)
    {
      face = TAROT_KING;
    }
  else
    {
      yarrow256_random (rng, sizeof (random_value), (void *) &random_value);
      face = mini_face + random_value % (TAROT_KING - mini_face + 1);
    }
  yarrow256_random (rng, sizeof (random_value), (void *) &random_value);
  suit = suits[random_value % 4];
  if (of (face, suit, &call) != 0)
    {
      assert (0);
    }
  event_set_call (event, call);
}

static void
select_next_move_dog (TarotGameEvent * event, const TarotGame * imputed)
{
  size_t n_dog;
  TarotCard dog[6];
  static const size_t max = sizeof (dog) / sizeof (dog[0]);
  int can_autoreveal = game_can_autoreveal (imputed, &n_dog, 0, max, dog);
  if (can_autoreveal)
    {
      event_set_dog (event, n_dog, dog);
    }
  else
    {
      assert (0);
    }
}

static void
shuffle (size_t n, TarotCard * set, struct yarrow256_ctx *rng)
{
  size_t random_value;
  size_t i;
  for (i = 1; i < n; i++)
    {
      size_t j;
      TarotCard hold;
      yarrow256_random (rng, sizeof (random_value), (void *) &random_value);
      j = random_value % (i + 1);
      hold = set[i];
      set[i] = set[j];
      set[j] = hold;
    }
}

static void
sample (size_t n_samplable, TarotCard * set, size_t n,
        struct yarrow256_ctx *rng, size_t *n_selected, int *selected)
{
  size_t i;
  shuffle (n_samplable, set, rng);
  for (i = 0; i < n_samplable; i++)
    {
      if (*n_selected < n)
        {
          TarotCard c = set[i];
          if (selected[c] == 0)
            {
              selected[c] = 1;
              *n_selected += 1;
            }
        }
    }
}

static void
select_next_move_discard (TarotGameEvent * event, size_t n, size_t n_prio,
                          TarotCard * prio, size_t n_additional,
                          TarotCard * additional, struct yarrow256_ctx *rng)
{
  int selected[78];
  size_t n_selected, i;
  TarotCard discard[6];
  for (i = 0; i < 78; i++)
    {
      selected[i] = 0;
    }
  n_selected = 0;
  sample (n_prio, prio, n, rng, &n_selected, selected);
  if (n_selected < n)
    {
      sample (n_additional, additional, n - n_selected, rng, &n_selected,
              selected);
    }
  assert (n_selected == n);
  n_selected = 0;
  for (i = 0; i < 78; i++)
    {
      if (selected[i])
        {
          assert (n_selected < sizeof (discard) / sizeof (discard[0]));
          discard[n_selected++] = i;
        }
    }
  assert (n_selected == n);
  event_set_discard (event, n, discard);
}

static void
select_next_move_card (TarotGameEvent * event,
                       size_t n_playable, TarotCard * playable,
                       struct yarrow256_ctx *rng)
{
  size_t i;
  size_t random_value;
  yarrow256_random (rng, sizeof (random_value), (void *) &random_value);
  i = random_value % n_playable;
  event_set_card (event, playable[i]);
}

static inline int
mcts_next_move (const TarotGame * game, TarotGameEvent * event,
                struct yarrow256_ctx *rng)
{
  TarotBid mini_bid;
  int decl_allowed;
  TarotPlayer next;
  TarotNumber mini_face;
  TarotCard prio[78];
  static const size_t max_prio = sizeof (prio) / sizeof (prio[0]);
  size_t n_prio;
  TarotCard additional[78];
  static const size_t max_additional =
    sizeof (additional) / sizeof (additional[0]);
  size_t n_additional;
  TarotCard playable[78];
  static const size_t max_playable = sizeof (playable) / sizeof (playable[0]);
  size_t n_playable;
  size_t n_cards;
  int ret = 0;
  switch (game_step (game))
    {
    case TAROT_SETUP:
      /* Since we have at least one event in the past, and we cannot
         have two consecutive setup events, then this is not
         possible */
      assert (0);
      break;
    case TAROT_DEAL:
      select_next_move_deal (game_n_players (game), event, rng);
      if (!game_check_event (game, event))
        {
          /* Petit sec */
          ret = 1;
        }
      break;
    case TAROT_BIDS:
      if (game_get_hint_bid (game, &next, &mini_bid) != TAROT_GAME_OK)
        {
          assert (0);
        }
      select_next_move_bid (event, mini_bid, rng);
      break;
    case TAROT_DECLS:
      if (game_get_hint_decl (game, &next, &decl_allowed) != TAROT_GAME_OK)
        {
          assert (0);
        }
      select_next_move_decl (event, decl_allowed, rng);
      break;
    case TAROT_CALL:
      if (game_get_hint_call (game, &next, &mini_face) != TAROT_GAME_OK)
        {
          assert (0);
        }
      select_next_move_call (event, mini_face, rng);
      break;
    case TAROT_DOG:
      select_next_move_dog (event, game);
      break;
    case TAROT_DISCARD:
      if (game_get_hint_discard
          (game, &next, &n_cards, &n_prio, 0, max_prio, prio, &n_additional,
           0, max_additional, additional) != TAROT_GAME_OK)
        {
          assert (0);
        }
      select_next_move_discard (event, n_cards, n_prio, prio, n_additional,
                                additional, rng);
      break;
    case TAROT_TRICKS:
      if (tarot_game_get_hint_card
          (game, &next, &n_playable, 0, max_playable,
           playable) != TAROT_GAME_OK)
        {
          assert (0);
        }
      select_next_move_card (event, n_playable, playable, rng);
      break;
    case TAROT_END:
      /* We don't select a move if we're at end */
      assert (0);
      break;
    }
  return ret;
}

static inline int
find_strongest (const TarotPlayer * owner, TarotSuit lead_suit,
                size_t n_players, TarotCard * strongest_following,
                TarotCard * strongest_trump, TarotCard * least_valuable,
                TarotCard * most_valuable)
{
  size_t i, n_of_lead_suit;
  TarotCard first, c;
  if (of (1, lead_suit, &first) != 0)
    {
      assert (0);
    }
  n_of_lead_suit = TAROT_KING;
  if (lead_suit == TAROT_TRUMPS)
    {
      n_of_lead_suit = 21;
    }
  for (i = 0; i < n_players; i++)
    {
      strongest_following[i] = ((TarotCard) (-1));
      strongest_trump[i] = ((TarotCard) (-1));
      least_valuable[i] = ((TarotCard) (-1));
      most_valuable[i] = ((TarotCard) (-1));
      for (c = first + n_of_lead_suit; c > first && owner[c - 1] != i; c--)
        ;
      if (c > first)
        {
          strongest_following[i] = c - 1;
        }
      if (lead_suit == TAROT_TRUMPS
          || strongest_following[i] == ((TarotCard) (-1)))
        {
          for (c = TAROT_TWENTYONE + 1; c > TAROT_PETIT && owner[c - 1] != i;
               c--)
            ;
          if (c > TAROT_PETIT)
            {
              strongest_trump[i] = c - 1;
              least_valuable[i] = c - 1;
            }
          for (c = TAROT_PETIT + 1; c < TAROT_TWENTYONE && owner[c] != i; c++)
            ;
          if (c < TAROT_TWENTYONE)
            {
              least_valuable[i] = c;
            }
          if (owner[TAROT_PETIT] == i)
            {
              most_valuable[i] = TAROT_PETIT;
              if (least_valuable[i] == ((TarotCard) (-1)))
                {
                  least_valuable[i] = TAROT_PETIT;
                }
            }
          else if (owner[TAROT_TWENTYONE] == i)
            {
              most_valuable[i] = TAROT_TWENTYONE;
              if (least_valuable[i] == ((TarotCard) (-1)))
                {
                  least_valuable[i] = TAROT_TWENTYONE;
                }
            }
          else if (least_valuable[i] != ((TarotCard) (-1)))
            {
              most_valuable[i] = least_valuable[i];
            }
          else
            {
              size_t i_valuable;
              /* I can play any card I like! */
              static const TarotCard most_valuable_order[4 * TAROT_KING] = {
                TAROT_KH, TAROT_KC, TAROT_KD, TAROT_KS,
                TAROT_QH, TAROT_QC, TAROT_QD, TAROT_QS,
                TAROT_CH, TAROT_CC, TAROT_CD, TAROT_CS,
                TAROT_JH, TAROT_JC, TAROT_JD, TAROT_JS,
                TAROT_AH, TAROT_AC, TAROT_AD, TAROT_AS,
                TAROT_2H, TAROT_2C, TAROT_2D, TAROT_2S,
                TAROT_3H, TAROT_3C, TAROT_3D, TAROT_3S,
                TAROT_4H, TAROT_4C, TAROT_4D, TAROT_4S,
                TAROT_5H, TAROT_5C, TAROT_5D, TAROT_5S,
                TAROT_6H, TAROT_6C, TAROT_6D, TAROT_6S,
                TAROT_7H, TAROT_7C, TAROT_7D, TAROT_7S,
                TAROT_8H, TAROT_8C, TAROT_8D, TAROT_8S,
                TAROT_9H, TAROT_9C, TAROT_9D, TAROT_9S,
                TAROT_10H, TAROT_10C, TAROT_10D, TAROT_10S
              };
              static const TarotCard least_valuable_order[4 * TAROT_KING] = {
                TAROT_AH, TAROT_AC, TAROT_AD, TAROT_AS,
                TAROT_2H, TAROT_2C, TAROT_2D, TAROT_2S,
                TAROT_3H, TAROT_3C, TAROT_3D, TAROT_3S,
                TAROT_4H, TAROT_4C, TAROT_4D, TAROT_4S,
                TAROT_5H, TAROT_5C, TAROT_5D, TAROT_5S,
                TAROT_6H, TAROT_6C, TAROT_6D, TAROT_6S,
                TAROT_7H, TAROT_7C, TAROT_7D, TAROT_7S,
                TAROT_8H, TAROT_8C, TAROT_8D, TAROT_8S,
                TAROT_9H, TAROT_9C, TAROT_9D, TAROT_9S,
                TAROT_10H, TAROT_10C, TAROT_10D, TAROT_10S,
                TAROT_JH, TAROT_JC, TAROT_JD, TAROT_JS,
                TAROT_CH, TAROT_CC, TAROT_CD, TAROT_CS,
                TAROT_QH, TAROT_QC, TAROT_QD, TAROT_QS,
                TAROT_KH, TAROT_KC, TAROT_KD, TAROT_KS
              };
              for (i_valuable = 0;
                   i_valuable < 4 * TAROT_KING
                   && owner[most_valuable_order[i_valuable]] != i;
                   i_valuable++)
                ;
              assert (i_valuable < 4 * TAROT_KING
                      || owner[TAROT_EXCUSE] == i);
              if (i_valuable < 4 * TAROT_KING)
                {
                  most_valuable[i] = most_valuable_order[i_valuable];
                }
              for (i_valuable = 0;
                   i_valuable < 4 * TAROT_KING
                   && owner[least_valuable_order[i_valuable]] != i;
                   i_valuable++)
                ;
              assert (i_valuable < 4 * TAROT_KING
                      || owner[TAROT_EXCUSE] == i);
              if (i_valuable < 4 * TAROT_KING)
                {
                  least_valuable[i] = least_valuable_order[i_valuable];
                }
            }
          if (lead_suit == TAROT_TRUMPS)
            {
              strongest_following[i] = strongest_trump[i];
            }
        }
      else
        {
          for (c = first; c < first + n_of_lead_suit && owner[c] != i; c++)
            ;
          if (c < first + n_of_lead_suit)
            {
              least_valuable[i] = c;
            }
          for (c = first + n_of_lead_suit; c > first && owner[c - 1] != i;
               c--)
            ;
          if (c > first)
            {
              most_valuable[i] = c - 1;
            }
        }
    }
  return 0;
}

static inline int
play_trick (TarotPlayer * owner, TarotPlayer taker, TarotPlayer partner,
            size_t random, TarotGame * game, TarotGameEvent * event)
{
  size_t n_players = tarot_game_n_players (game);
  static const TarotSuit suits[5] = {
    TAROT_HEARTS, TAROT_CLUBS, TAROT_DIAMONDS, TAROT_SPADES, TAROT_TRUMPS
  };
  size_t try_suit;
  TarotCard strongest_following[5];
  TarotCard strongest_trump[5];
  TarotCard least_valuable[5];
  TarotCard most_valuable[5];
  TarotPlayer trick_taker = 0, i, trick_leader;
  assert (n_players <= 5);
  assert (partner < n_players);
  if (game_get_next (game, &trick_leader) != TAROT_GAME_OK)
    {
      assert (0);
    }
  for (try_suit = 0; try_suit < 5; try_suit++)
    {
      TarotSuit lead_suit = suits[(try_suit + random) % 5];
      int forbidden_suit = 0;
      size_t current_trick;
      int has_current_trick =
        (game_get_current_trick (game, &current_trick) == TAROT_GAME_OK);
      assert (has_current_trick);
      if (current_trick == 0 && game_with_call (game))
        {
          TarotCard call_card;
          TarotNumber call_number;
          TarotSuit call_suit;
          if (game_get_call (game, &call_card) != TAROT_GAME_OK)
            {
              assert (0);
            }
          if (decompose (call_card, &call_number, &call_suit) != 0)
            {
              assert (0);
            }
          forbidden_suit = (call_suit == lead_suit);
        }
      if (!forbidden_suit)
        {
          if (find_strongest
              (owner, lead_suit, n_players, strongest_following,
               strongest_trump, least_valuable, most_valuable) != 0)
            {
              return 1;
            }
        }
      if (forbidden_suit)
        {
          /* This is the called suit.  Forbidden. */
        }
      else if (strongest_following[trick_leader] == ((TarotCard) (-1)))
        {
          /* It is not possible that lead_suit be the lead suit, try another one */
        }
      else
        {
          TarotCard max_trump = 0;
          trick_taker = 0;
          TarotCard strongest_card;
          strongest_card = strongest_following[0];
          if (strongest_card == ((TarotCard) (-1)))
            {
              strongest_card = strongest_trump[0];
            }
          for (i = 1; i < n_players; i++)
            {
              if (strongest_card == ((TarotCard) (-1))
                  || (strongest_following[i] != ((TarotCard) (-1))
                      && strongest_following[i] > strongest_card))
                {
                  strongest_card = strongest_following[i];
                  trick_taker = i;
                }
              if (strongest_card == ((TarotCard) (-1))
                  || (strongest_following[i] == ((TarotCard) (-1))
                      && strongest_trump[i] != ((TarotCard) (-1))
                      && strongest_trump[i] > strongest_card))
                {
                  strongest_card = strongest_trump[i];
                  trick_taker = i;
                }
            }
          for (i = 0; i < n_players; i++)
            {
              TarotPlayer who;
              TarotCard played;
              if (game_get_next (game, &who) != TAROT_GAME_OK)
                {
                  assert (0);
                }
              if (who == trick_taker)
                {
                  played = strongest_following[who];
                  if (played == ((TarotCard) (-1)))
                    {
                      played = strongest_trump[who];
                    }
                }
              else if ((who == taker || who == partner) ==
                       (trick_taker == taker || trick_taker == partner))
                {
                  played = most_valuable[who];
                }
              else
                {
                  if (who != trick_leader && owner[TAROT_EXCUSE] == who)
                    {
                      played = TAROT_EXCUSE;
                    }
                  else
                    {
                      played = least_valuable[who];
                    }
                }
              if (played >= TAROT_PETIT && played <= TAROT_TWENTYONE
                  && played < max_trump
                  && strongest_trump[who] != ((TarotCard) (-1))
                  && strongest_trump[who] > max_trump)
                {
                  /* This is rare.  Either we make another round of
                     finding the least / most valuable AT LEAST
                     max_trump, or we play our strongest card.  If
                     we're refused to play the petit, playing our
                     strongest trump is OK. */
                  played = strongest_trump[who];
                }
              assert (played != ((TarotCard) (-1)));
              event_set_card (event, played);
              if (game_add_event (game, event) != TAROT_GAME_OK)
                {
                  assert (0);
                }
              owner[played] = ((TarotPlayer) (-1));
              if (played >= TAROT_PETIT && played <= TAROT_TWENTYONE
                  && played > max_trump)
                {
                  max_trump = played;
                }
            }
          return 0;
        }
    }
  return 1;
}

static inline int
mcts_simulate_tricks (TarotGame * game, struct yarrow256_ctx *prng,
                      TarotGameEvent * event)
{
  TarotPlayer owner[78];
  size_t i = 0;
  size_t n_players = tarot_game_n_players (game);
  TarotPlayer taker, partner;
  size_t current_trick;
  if (game_get_taker (game, &taker) != TAROT_GAME_OK)
    {
      return 1;
    }
  if (game_get_partner (game, &partner) != TAROT_GAME_OK)
    {
      partner = taker;
    }
  assert (taker < n_players);
  assert (partner < n_players);
  for (i = 0; i < 78; i++)
    {
      owner[i] = ((TarotPlayer) (-1));
    }
  for (i = 0; i < n_players; i++)
    {
      size_t n_cards;
      size_t i_card;
      TarotCard cards[78];
      static const size_t max_cards = sizeof (cards) / sizeof (cards[0]);
      if (game_get_cards (game, i, &n_cards, 0, max_cards, cards) != 0)
        {
          return 1;
        }
      for (i_card = 0; i_card < n_cards; i_card++)
        {
          owner[cards[i_card]] = i;
        }
    }
  while (game_step (game) != TAROT_END
         && game_get_current_trick (game, &current_trick) == TAROT_GAME_OK
         && current_trick + 1 < game_n_tricks (game))
    {
      unsigned int r;
      yarrow256_random (prng, sizeof (r), (unsigned char *) &r);
      if (play_trick (owner, taker, partner, r, game, event) != 0)
        {
          return 1;
        }
    }
  return 0;
}

static TarotSimulationError
simulate (struct yarrow256_ctx *prng, TarotGame * game,
          int with_tricks_agreed)
{
  int error = 0;
  unsigned int event_data[78];
  TarotGameEvent event_header = {.data = event_data };
  TarotGameEvent *event = &event_header;
  size_t n_trivial_moves = 0;
  while (error == 0 && game_step (game) != TAROT_END)
    {
      size_t current_trick;
      int no_cards_in_trick = 0;
      if (game_get_current_trick (game, &current_trick) == TAROT_GAME_OK)
        {
          if (game_step (game) == TAROT_TRICKS
              && current_trick + 1 < game_n_tricks (game))
            {
              size_t n_cards_in_trick;
              if (game_get_trick_cards
                  (game, current_trick, &n_cards_in_trick, 0, 0,
                   NULL) != TAROT_GAME_OK)
                {
                  assert (0);
                }
              if (n_cards_in_trick == 0)
                {
                  no_cards_in_trick = 1;
                }
            }
        }
      if (with_tricks_agreed && no_cards_in_trick)
        {
          error = mcts_simulate_tricks (game, prng, event);
        }
      else
        {
          n_trivial_moves++;
          error = mcts_next_move (game, event, prng);
          if (game_add_event (game, event) != 0)
            {
              assert (0);
            }
        }
    }
  /* 5 bids, 5 decls, call, discard, finish the first trick (no
     handfuls allowed); the rest should be simulated tricks. */
  assert (with_tricks_agreed == 0 || n_trivial_moves < 17);
  if (error == 0)
    {
      return TAROT_SIMULATION_OK;
    }
  return TAROT_SIMULATION_INVSEED;
}

TarotSimulationError
tarot_simulate (struct yarrow256_ctx *prng, struct TarotGame *game)
{
  if (TUNE_SIMULATION_RANDOM >= TUNE_SIMULATION_AGREED)
    {
      return simulate (prng, game, 0);
    }
  return simulate (prng, game, 1);
}

TarotSimulationError
tarot_simulate_random (struct yarrow256_ctx *prng, struct TarotGame *game)
{
  return simulate (prng, game, 0);
}

TarotSimulationError
tarot_simulate_agreed (struct yarrow256_ctx *prng, struct TarotGame *game)
{
  return simulate (prng, game, 1);
}

#include <tarot/cards_private_impl.h>
#include <tarot/game_private_impl.h>
#include <tarot/game_event_private_impl.h>
#include <tarot/game_private_impl.h>

/* Local Variables: */
/* mode: c */
/* End: */
