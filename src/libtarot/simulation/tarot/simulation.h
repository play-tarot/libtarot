/*
 * file simulation.h
 *
 * Copyright (C) 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
#ifndef H_TAROT_SIMULATION_INCLUDED
#define H_TAROT_SIMULATION_INCLUDED
#include <stddef.h>
#include <tarot/player.h>
#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct yarrow256_ctx;
  struct TarotGame;
  typedef struct TarotGame TarotGame;

  typedef enum
  {
    TAROT_SIMULATION_OK,
    TAROT_SIMULATION_INVSEED
  } TarotSimulationError;

  /**
   * tarot_simulate:
   */
  TarotSimulationError tarot_simulate (struct yarrow256_ctx *prng,
                                       TarotGame *game);

  /**
   * tarot_simulate_random:
   */
  TarotSimulationError tarot_simulate_random (struct yarrow256_ctx *prng,
                                              TarotGame *game);

  /**
   * tarot_simulate_agreed:
   */
  TarotSimulationError tarot_simulate_agreed (struct yarrow256_ctx *prng,
					      TarotGame *game);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_SIMULATION_INCLUDED */

/* Local Variables: */
/* mode: c */
/* End: */
