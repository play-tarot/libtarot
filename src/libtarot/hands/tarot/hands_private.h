/**
 * file tarot/hands_private.h libtarot header for the management of
 * all cards of all players in a game.
 *
 * Copyright (C) 2017, 2018, 2019 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_HANDS_PRIVATE_INCLUDED
#define H_TAROT_HANDS_PRIVATE_INCLUDED

#include <tarot_private.h>
#include <tarot/cards.h>
#include <tarot/player.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void hands_3_generalize (TarotHands3 * hands,
                                  TarotHandsHandle * handle);
  static void hands_4_generalize (TarotHands4 * hands,
                                  TarotHandsHandle * handle);
  static void hands_5_generalize (TarotHands5 * hands,
                                  TarotHandsHandle * handle);

  static void hands_initialize (TarotHandsHandle * hands, size_t n_players);
  static void hands_copy (TarotHandsHandle * dest,
                          const TarotHandsHandle * source);
  static int hands_known (const TarotHandsHandle * hands, TarotPlayer player);
  static int hands_set (TarotHandsHandle * hands, TarotPlayer player,
                        const TarotHand * hand);
  static int hands_set_unknown (TarotHandsHandle * hands, TarotPlayer player);
  static int hands_set_all (TarotHandsHandle * hands,
                            const TarotPlayer * owner);
  static void hands_remove (TarotHandsHandle * hands, TarotPlayer player,
                            TarotCard card);
  static int hands_assign_missing_card (TarotHandsHandle * hands,
                                        TarotCard card, TarotPlayer owner,
                                        size_t n_remaining);
  static int hands_locate (const TarotHandsHandle * hands, TarotCard card,
                           TarotPlayer * OUTPUT);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_HANDS_PRIVATE_INCLUDED */
