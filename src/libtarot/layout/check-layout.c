/**
 * file check-layout.c Check that the layout is correct.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "xalloc.h"
#include <assert.h>

static const size_t skipped[] = {
  0, 1, 3, 2
};

static void
print_svg (const char *description, double view_w, double view_h,
           double card_w, double card_h, size_t n_my_cards, size_t n_center)
{
  TarotPosition *positions = NULL;
  size_t n_positions =
    tarot_layout_position (view_w, view_h, card_w, card_h, n_my_cards,
                           sizeof (skipped) / sizeof (skipped[0]), skipped,
                           n_center, 0, NULL);
  size_t i;
  size_t n_positions_2;
  positions = xmalloc (n_positions * sizeof (TarotPosition));
  n_positions_2 =
    tarot_layout_position (view_w, view_h, card_w, card_h, n_my_cards,
                           sizeof (skipped) / sizeof (skipped[0]), skipped,
                           n_center, n_positions, positions);
  if (n_positions_2 != n_positions)
    {
      fprintf (stderr, "%s:%d: %lu != %lu\n", __FILE__, __LINE__,
               n_positions_2, n_positions);
      assert (0);
    }
  printf
    ("<test description=\"%s (everything)\" width=\"%f\" height=\"%f\" card-width=\"%f\" card-height=\"%f\" n-cards=\"%lu\">",
     description, view_w, view_h, card_w, card_h, n_positions);
  for (i = 0; i < n_positions; i++)
    {
      printf
        ("<card index=\"%lu\" x=\"%f\" y=\"%f\" z=\"%f\" angle=\"%f\" scale=\"%f\" />",
         i, positions[i].x, positions[i].y, positions[i].z,
         positions[i].angle, positions[i].scale);
    }
  printf ("</test>");
  free (positions);
  tarot_layout_hand_alloc (view_w, view_h, card_w, card_h, n_my_cards,
                           &n_positions, &positions);
  printf
    ("<test description=\"%s (only the hand)\" width=\"%f\" height=\"%f\" card-width=\"%f\" card-height=\"%f\" n-cards=\"%lu\">",
     description, view_w, view_h, card_w, card_h, n_positions);
  for (i = 0; i < n_positions; i++)
    {
      printf
        ("<card index=\"%lu\" x=\"%f\" y=\"%f\" z=\"%f\" angle=\"%f\" scale=\"%f\" />",
         i, positions[i].x, positions[i].y, positions[i].z,
         positions[i].angle, positions[i].scale);
    }
  printf ("</test>");
  free (positions);
  tarot_layout_trick_alloc (view_w, view_h, card_w, card_h, 5, &n_positions,
                            &positions);
  printf
    ("<test description=\"%s (only the trick)\" width=\"%f\" height=\"%f\" card-width=\"%f\" card-height=\"%f\" n-cards=\"%lu\">",
     description, view_w, view_h, card_w, card_h, n_positions);
  for (i = 0; i < n_positions; i++)
    {
      printf
        ("<card index=\"%lu\" x=\"%f\" y=\"%f\" z=\"%f\" angle=\"%f\" scale=\"%f\" />",
         i, positions[i].x, positions[i].y, positions[i].z,
         positions[i].angle, positions[i].scale);
    }
  printf ("</test>");
  free (positions);
  tarot_layout_center_alloc (view_w, view_h, card_w, card_h, n_center,
                             &n_positions, &positions);
  printf
    ("<test description=\"%s (only the center)\" width=\"%f\" height=\"%f\" card-width=\"%f\" card-height=\"%f\" n-cards=\"%lu\">",
     description, view_w, view_h, card_w, card_h, n_positions);
  for (i = 0; i < n_positions; i++)
    {
      printf
        ("<card index=\"%lu\" x=\"%f\" y=\"%f\" z=\"%f\" angle=\"%f\" scale=\"%f\" />",
         i, positions[i].x, positions[i].y, positions[i].z,
         positions[i].angle, positions[i].scale);
    }
  printf ("</test>");
  free (positions);
}

int
main ()
{
  printf ("<?xml version=\"1.0\"?>\n");
  printf ("<?xml-stylesheet type=\"text/xsl\" href=\"check-layout.xsl\"?>\n");
  printf ("<example xmlns=\"http://planete-kraus.eu/tarot\">");
  print_svg ("normal", 640, 480, 48, 64, 10, 0);
  print_svg ("with a center", 640, 480, 48, 64, 10, 6);
  print_svg ("portrait mode", 480, 640, 48, 64, 10, 0);
  print_svg ("portrait, a center", 480, 640, 48, 64, 10, 6);
  print_svg ("very thin portrait", 480, 2400, 64, 32, 10, 6);
  printf ("</example>");
  return 0;
}
