/**
 * file tarot/layout.c
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <tarot/layout_private.h>
#include <stdio.h>
#include "xalloc.h"
#include <assert.h>

size_t
tarot_layout_position (double view_w, double view_h, double card_w,
                       double card_h, size_t n_my_cards, size_t n_adversaries,
                       const size_t *n_adv_cards, size_t n_center,
                       size_t max_dst, TarotPosition * dst)
{
  return layout_position (view_w, view_h, card_w, card_h, n_my_cards,
                          n_adversaries, n_adv_cards, n_center, max_dst, dst);
}

size_t
tarot_layout_hand (double view_w, double view_h, double card_w, double card_h,
                   size_t n_cards, size_t max, size_t start,
                   TarotPosition * position)
{
  return layout_hand (view_w, view_h, card_w, card_h, n_cards, max, start,
                      position);
}

size_t
tarot_layout_trick (double view_w, double view_h, double card_w,
                    double card_h, size_t n_players, size_t max, size_t start,
                    TarotPosition * position)
{
  return layout_trick (view_w, view_h, card_w, card_h, n_players, max, start,
                       position);
}

size_t
tarot_layout_center (double view_w, double view_h, double card_w,
                     double card_h, size_t n_center, size_t max, size_t start,
                     TarotPosition * position)
{
  return layout_center (view_w, view_h, card_w, card_h, n_center, max, start,
                        position);
}

void
tarot_layout_hand_alloc (double view_w, double view_h, double card_w,
                         double card_h, size_t n_cards, size_t *n_positions,
                         TarotPosition ** positions)
{
  size_t required = layout_hand (view_w, view_h, card_w, card_h,
                                 n_cards, 0, 0, NULL);
  *positions = xmalloc (required * sizeof (TarotPosition));
  *n_positions = required;
  required = layout_hand (view_w, view_h, card_w, card_h,
                          n_cards, required, 0, *positions);
  assert (required == *n_positions);
}

void
tarot_layout_trick_alloc (double view_w, double view_h, double card_w,
                          double card_h, size_t n_players,
                          size_t *n_positions, TarotPosition ** positions)
{
  size_t required = layout_trick (view_w, view_h, card_w, card_h,
                                  n_players, 0, 0, NULL);
  *positions = xmalloc (required * sizeof (TarotPosition));
  *n_positions = required;
  required = layout_trick (view_w, view_h, card_w, card_h,
                           n_players, required, 0, *positions);
  assert (required == *n_positions);
}

void
tarot_layout_center_alloc (double view_w, double view_h, double card_w,
                           double card_h, size_t n_cards, size_t *n_positions,
                           TarotPosition ** positions)
{
  size_t required = layout_center (view_w, view_h, card_w, card_h,
                                   n_cards, 0, 0, NULL);
  *positions = xmalloc (required * sizeof (TarotPosition));
  *n_positions = required;
  required = layout_center (view_w, view_h, card_w, card_h,
                            n_cards, required, 0, *positions);
  assert (required == *n_positions);
}

#include <tarot/layout_private_impl.h>
