/*
 * file tarot/layout.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_LAYOUT_INCLUDED
#define H_TAROT_LAYOUT_INCLUDED

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotPosition
  {
    double x;
    double y;
    double z;
    double scale;
    double angle;
  };
  typedef struct TarotPosition TarotPosition;

  struct TarotCollision
  {
    double x;
    double y;
    size_t card_index;
  };
  typedef struct TarotCollision TarotCollision;

  /**
   * tarot_layout_position:
   * @view_w: the width of the view
   * @view_h: the height of the view
   * @card_w: the width of each card
   * @card_h: the height of each card
   * @n_my_cards: the number of cards in my hand
   * @n_adversaries: the number of adversaries
   * @n_adv_cards: (array length=n_adversaries): the number of cards
   * for each adversary
   * @n_center: the number of cards in the center (dog / discard, handful)
   * @max_dst: the number of positions
   * @dst: (array length=max_dst): where to store the positions
   * Returns: the number of cards to position (n_my_cards + sum
   * (n_adv_cards) + n_adversaries + 1 + n_center)
   */
  size_t tarot_layout_position (double view_w, double view_h, double card_w,
                                double card_h, size_t n_my_cards,
                                size_t n_adversaries,
                                const size_t *n_adv_cards, size_t n_center,
                                size_t max_dst, TarotPosition * dst);

  /**
   * tarot_layout_hand:
   * @position: (array length=max):
   */
  size_t tarot_layout_hand (double view_w, double view_h, double card_w,
                            double card_h, size_t n_cards, size_t max,
                            size_t start, TarotPosition * position);

  /**
   * tarot_layout_trick:
   * @position: (array length=max):
   */
  size_t tarot_layout_trick (double view_w, double view_h, double card_w,
                             double card_h, size_t n_players, size_t max,
                             size_t start, TarotPosition * position);

  /**
   * tarot_layout_center:
   * @position: (array length=max):
   */
  size_t tarot_layout_center (double view_w, double view_h, double card_w,
                              double card_h, size_t n_cards, size_t max,
                              size_t start, TarotPosition * position);

  /**
   * tarot_layout_hand_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_hand_alloc (double view_w, double view_h, double card_w,
                                double card_h, size_t n_cards,
                                size_t *n_positions,
                                TarotPosition ** positions);

  /**
   * tarot_layout_trick_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_trick_alloc (double view_w, double view_h, double card_w,
                                 double card_h, size_t n_players,
                                 size_t *n_positions,
                                 TarotPosition ** positions);

  /**
   * tarot_layout_center_alloc:
   * @positions: (out) (array length=n_positions):
   */
  void tarot_layout_center_alloc (double view_w, double view_h, double card_w,
                                  double card_h, size_t n_cards,
                                  size_t *n_positions,
                                  TarotPosition ** positions);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_LAYOUT_INCLUDED */
