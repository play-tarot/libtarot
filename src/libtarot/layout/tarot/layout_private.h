/**
 * file tarot/layout_private.h
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_LAYOUT_PRIVATE_INCLUDED
#define H_TAROT_LAYOUT_PRIVATE_INCLUDED

#ifdef HAVE_MATH_H
#include <math.h>
#endif /* HAVE_MATH_H */
#ifdef HAVE_TGMATH_H
#include <tgmath.h>
#endif /* HAVE_TGMATH_H */

#define ALPHA 0.25

#include <tarot/layout.h>

static size_t layout_position (double view_w, double view_h, double card_w,
                               double card_h, size_t n_my_cards,
                               size_t n_adversaries,
                               const size_t *n_adv_cards, size_t n_center,
                               size_t max_dst, TarotPosition * dst);

static size_t layout_hand (double view_w, double view_h, double card_w,
                           double card_h, size_t n_cards, size_t max,
                           size_t start, TarotPosition * position);

static size_t layout_trick (double view_w, double view_h, double card_w,
                            double card_h, size_t n_players, size_t max,
                            size_t start, TarotPosition * position);

static size_t layout_center (double view_w, double view_h, double card_w,
                             double card_h, size_t n_center, size_t max,
                             size_t start, TarotPosition * position);

#endif /* not H_TAROT_LAYOUT_PRIVATE_INCLUDED */
