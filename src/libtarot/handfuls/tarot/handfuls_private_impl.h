/**
 * file tarot/handfuls_private_impl.h libtarot code for the management
 * of the handfuls.
 *
 * Copyright (C) 2017, 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <tarot/handfuls_private.h>

static inline int
_handfuls_get_has_started (const TarotHandfuls * handfuls)
{
  return handfuls->has_started;
}

static inline void
_handfuls_set_has_started (TarotHandfuls * handfuls, int value)
{
  handfuls->has_started = value;
}

static inline size_t
_handfuls_get_n_decisions (const TarotHandfuls * handfuls)
{
  return handfuls->n_decisions;
}

static inline void
_handfuls_set_n_decisions (TarotHandfuls * handfuls, size_t value)
{
  handfuls->n_decisions = value;
}

static inline int
_handfuls_get_has_next (const TarotHandfuls * handfuls)
{
  return handfuls->has_next;
}

static inline void
_handfuls_set_has_next (TarotHandfuls * handfuls, int value)
{
  handfuls->has_next = value;
}

static inline TarotPlayer
_handfuls_get_leader (const TarotHandfuls * handfuls)
{
  return handfuls->leader;
}

static inline void
_handfuls_set_leader (TarotHandfuls * handfuls, TarotPlayer value)
{
  handfuls->leader = value;
}

static inline TarotPlayer
_handfuls_get_shower_of (const TarotHandfuls * handfuls, TarotNumber n)
{
  return handfuls->shower_of[n];
}

static inline void
_handfuls_set_shower_of (TarotHandfuls * handfuls, TarotNumber n,
                         TarotPlayer value)
{
  handfuls->shower_of[n] = value;
}

static inline void
_handfuls_set_shower_of_excuse (TarotHandfuls * handfuls, TarotPlayer value)
{
  _handfuls_set_shower_of (handfuls, 0, value);
}

static inline void
handfuls_initialize (TarotHandfuls * handfuls, size_t n_players)
{
  TarotNumber i;
  _handfuls_set_has_started (handfuls, 0);
  _handfuls_set_n_decisions (handfuls, 0);
  _handfuls_set_has_next (handfuls, 1);
  _handfuls_set_leader (handfuls, 0);
  for (i = 0; i <= 21; ++i)
    {
      _handfuls_set_shower_of (handfuls, i, n_players);
    }
}

static inline int
handfuls_copy (TarotHandfuls * dest, const TarotHandfuls * source)
{
  TarotNumber i;
  _handfuls_set_has_started (dest, _handfuls_get_has_started (source));
  _handfuls_set_n_decisions (dest, _handfuls_get_n_decisions (source));
  _handfuls_set_has_next (dest, _handfuls_get_has_next (source));
  _handfuls_set_leader (dest, _handfuls_get_leader (source));
  for (i = 0; i <= 21; ++i)
    {
      _handfuls_set_shower_of (dest, i, _handfuls_get_shower_of (source, i));
    }
  return 0;
}

static inline int
handfuls_start (TarotHandfuls * hf)
{
  int error = (_handfuls_get_has_started (hf) != 0);
  if (error == 0)
    {
      _handfuls_set_has_started (hf, 1);
      _handfuls_set_has_next (hf, 1);
    }
  return error;
}

static inline TarotPlayer
handfuls_leader (const TarotHandfuls * hf)
{
  return _handfuls_get_leader (hf);
}

static inline size_t
handfuls_n_decisions (const TarotHandfuls * hf)
{
  return _handfuls_get_n_decisions (hf);
}

static inline int
handfuls_has_handful (const TarotHandfuls * handfuls, TarotPlayer p,
                      size_t n_players)
{
  size_t n = _handfuls_get_n_decisions (handfuls);
  int p_pos = p - _handfuls_get_leader (handfuls);
  if (p_pos < 0)
    {
      p_pos += n_players;
    }
  return (_handfuls_get_has_started (handfuls) && p_pos < (int) n);
}

static inline int
handfuls_has_next (const TarotHandfuls * hf)
{
  return _handfuls_get_has_started (hf) && _handfuls_get_has_next (hf);
}

static inline TarotPlayer
handfuls_next (const TarotHandfuls * hf, size_t n_players)
{
  TarotPlayer ret =
    _handfuls_get_leader (hf) + _handfuls_get_n_decisions (hf);
  if (ret >= n_players)
    {
      return ret - n_players;
    }
  return ret;
}

static inline size_t
handfuls_handful_count_of (const TarotHandfuls * handfuls, TarotPlayer p)
{
  size_t ret = 0, i;
  for (i = 0; i < 22; ++i)
    {
      if (_handfuls_get_shower_of (handfuls, i) == p)
        {
          ret++;
        }
    }
  return ret;
}

static inline int
handfuls_handful_of (const TarotHandfuls * handfuls,
                     TarotPlayer p, TarotHand * dest)
{
  int error = 0;
  TarotNumber i;
  error = hand_set (dest, NULL, 0);
  for (i = 0; i < 22; ++i)
    {
      if (_handfuls_get_shower_of (handfuls, i) == p)
        {
          TarotCard c = TAROT_PETIT - 1 + i;
          if (i == 0)
            {
              c = TAROT_EXCUSE;
            }
          error = error || hand_insert (dest, c);
        }
    }
  return error;
}

static inline void
handfuls_sizes (size_t n_players, unsigned int *n)
{
  n[0] = 0;
  switch (n_players)
    {
    case 3:
      n[1] = 13;
      n[2] = 15;
      n[3] = 18;
      break;
    case 4:
      n[1] = 10;
      n[2] = 13;
      n[3] = 15;
      break;
    case 5:
      n[1] = 8;
      n[2] = 10;
      n[3] = 13;
      break;
    default:
      break;
    }
}

static inline int
handfuls_handful_size_of (const TarotHandfuls * handfuls, TarotPlayer p,
                          size_t n_players)
{
  unsigned int n[4] = { 0 };
  size_t i, n_shown;
  handfuls_sizes (n_players, n);
  n_shown = handfuls_handful_count_of (handfuls, p);
  for (i = 0; i < 4; ++i)
    {
      if (n_shown == n[i])
        {
          return i;
        }
    }
  return 0;
}

static inline int
handfuls_handful_size (const TarotHandfuls * handfuls, size_t i,
                       size_t n_players)
{
  TarotPlayer p = _handfuls_get_leader (handfuls) + i;
  if (p >= n_players)
    {
      p -= n_players;
    }
  return handfuls_handful_size_of (handfuls, p, n_players);
}

static inline int
handfuls_bonus (const TarotHandfuls * handfuls, size_t n_players)
{
  static const int bonus[4] = { 0, 20, 30, 40 };
  int ret = 0;
  TarotPlayer p;
  for (p = 0; p < n_players; ++p)
    {
      ret += bonus[handfuls_handful_size_of (handfuls, p, n_players)];
    }
  return ret;
}

static inline int
handfuls_check (const TarotHandfuls * handfuls, TarotPlayer p,
                const TarotHand * handful, size_t n_players)
{
  if (_handfuls_get_has_started (handfuls)
      && _handfuls_get_has_next (handfuls)
      && handfuls_next (handfuls, n_players) == p)
    {
      size_t i;
      unsigned int n[4];
      handfuls_sizes (n_players, n);
      for (i = 0; i < 4; ++i)
        {
          if (hand_size (handful) == n[i])
            {
              return 1;
            }
        }
    }
  return 0;
}

static inline int
handfuls_add (TarotHandfuls * handfuls, const TarotHand * hand,
              size_t n_players)
{
  TarotNumber i;
  if (!handfuls_check (handfuls, handfuls_next (handfuls, n_players),
                       hand, n_players))
    {
      return 1;
    }
  for (i = 1; i < 22; ++i)
    {
      if (hand_has (hand, TAROT_PETIT - 1 + i))
        {
          _handfuls_set_shower_of (handfuls, i,
                                   handfuls_next (handfuls, n_players));
        }
    }
  if (hand_has (hand, TAROT_EXCUSE))
    {
      _handfuls_set_shower_of_excuse (handfuls,
                                      handfuls_next (handfuls, n_players));
    }
  _handfuls_set_has_next (handfuls, 0);
  _handfuls_set_n_decisions (handfuls,
                             1 + _handfuls_get_n_decisions (handfuls));
  return 0;
}

static inline int
handfuls_card (TarotHandfuls * handfuls, size_t n_players)
{
  int error = !(_handfuls_get_has_started (handfuls));
  if (error == 0)
    {
      int new_has_next = 0;
      if (_handfuls_get_has_next (handfuls))
        {
          /* We skipped the handful */
          size_t old_n = _handfuls_get_n_decisions (handfuls);
          size_t new_n = old_n + 1;
          _handfuls_set_n_decisions (handfuls, new_n);
        }
      new_has_next = (_handfuls_get_n_decisions (handfuls) < n_players);
      _handfuls_set_has_next (handfuls, new_has_next);
    }
  return error;
}

static inline int
handfuls_set_leader (TarotHandfuls * handfuls, TarotPlayer leader)
{
  int error = _handfuls_get_has_started (handfuls);
  if (error == 0)
    {
      _handfuls_set_leader (handfuls, leader);
    }
  return error;
}
