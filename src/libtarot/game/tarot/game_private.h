/**
 * file tarot/game_private.h libtarot header for the management of a
 * game, private data types.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_PRIVATE_INCLUDED
#define H_TAROT_GAME_PRIVATE_INCLUDED

#include <tarot_private.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  static void game_initialize (TarotGame * dest, size_t n_players,
                               int with_call);
  static void game_copy (TarotGame * dest, const TarotGame * source);
  static TarotStep game_step (const TarotGame * game);
  static void game_iterator_setup (TarotGameIterator * iterator,
                                   const TarotGame * game);
  static size_t game_save_to_xml (const TarotGame * game, size_t start,
                                  size_t max, char *dest);
  static TarotGameError game_load_from_xml (TarotGame * game,
                                            const char *input);
  static size_t game_n_players (const TarotGame * game);
  static int game_with_call (const TarotGame * game);
  static TarotGameError game_get_next (const TarotGame * game,
                                       TarotPlayer * next);
  static TarotGameError game_get_main_player (const TarotGame * game,
                                              TarotPlayer * main);
  static TarotGameError game_get_deal_all (const TarotGame * game, size_t *n,
                                           size_t start, size_t max,
                                           TarotPlayer * owners);
  static TarotGameError game_get_deal_of (const TarotGame * game,
                                          TarotPlayer who, size_t *n,
                                          size_t start, size_t max,
                                          TarotCard * deal);
  static TarotGameError game_get_cards (const TarotGame * game,
                                        TarotPlayer who, size_t *n,
                                        size_t start, size_t max,
                                        TarotCard * cards);
  static TarotGameError game_get_bids (const TarotGame * game, size_t *n,
                                       TarotPlayer start, size_t max,
                                       TarotBid * bids);
  static TarotGameError game_get_taker (const TarotGame * game,
                                        TarotPlayer * taker);
  static TarotGameError game_get_declarations (const TarotGame * game,
                                               size_t *n, TarotPlayer start,
                                               size_t max, int *decls);
  static TarotGameError game_get_declarant (const TarotGame * game,
                                            TarotPlayer * declarant);
  static TarotGameError game_get_call (const TarotGame * game,
                                       TarotCard * card);
  static TarotGameError game_get_partner (const TarotGame * game,
                                          TarotPlayer * partner);
  static TarotGameError game_get_dog (const TarotGame * game, size_t *n,
                                      size_t start, size_t max,
                                      TarotCard * cards);
  static TarotGameError game_get_full_discard (const TarotGame * game,
                                               size_t *n, size_t start,
                                               size_t max, TarotCard * cards);
  static TarotGameError game_get_public_discard (const TarotGame * game,
                                                 size_t *n, size_t start,
                                                 size_t max,
                                                 TarotCard * cards);
  static TarotGameError game_get_handful (const TarotGame * game,
                                          TarotPlayer p, int *size,
                                          size_t *n_cards, size_t start,
                                          size_t max, TarotCard * cards);
  static size_t game_n_tricks (const TarotGame * game);
  static TarotGameError game_get_current_trick (const TarotGame * game,
                                                size_t *current);
  static TarotGameError game_get_lead_suit (const TarotGame * game,
                                            size_t i_trick,
                                            TarotSuit * lead_suit);
  static TarotGameError game_get_max_trump (const TarotGame * game,
                                            size_t i_trick,
                                            TarotNumber * max_trump);
  static TarotGameError game_get_trick_leader (const TarotGame * game,
                                               size_t i_trick,
                                               TarotPlayer * leader);
  static TarotGameError game_get_trick_taker (const TarotGame * game,
                                              size_t i_trick,
                                              TarotPlayer * taker);
  static TarotGameError game_get_trick_cards (const TarotGame * game,
                                              size_t i_trick,
                                              size_t *n_cards, size_t start,
                                              size_t max,
                                              TarotGameCardAnalysis * cards);
  static TarotGameError game_get_points_in_trick (const TarotGame * game,
                                                  size_t i_trick,
                                                  unsigned int *halfpoint,
                                                  unsigned int *oudlers);
  static TarotGameError game_get_scores (const TarotGame * game, size_t *n,
                                         size_t start, size_t max,
                                         int *scores);
  static TarotGameError game_get_hint_bid (const TarotGame * game,
                                           TarotPlayer * player,
                                           TarotBid * mini);
  static TarotGameError game_get_hint_decl (const TarotGame * game,
                                            TarotPlayer * player,
                                            int *allowed);
  static TarotGameError game_get_hint_call (const TarotGame * game,
                                            TarotPlayer * player,
                                            TarotNumber * mini);
  static TarotGameError game_get_hint_discard (const TarotGame * game,
                                               TarotPlayer * player,
                                               size_t *n_cards,
                                               size_t *n_prio,
                                               size_t start_prio,
                                               size_t max_prio,
                                               TarotCard * prio,
                                               size_t *n_additional,
                                               size_t start_additional,
                                               size_t max_additional,
                                               TarotCard * additional);
  static TarotGameError game_get_hint_handful (const TarotGame * game,
                                               TarotPlayer * player,
                                               size_t *n_simple,
                                               size_t *n_double,
                                               size_t *n_triple,
                                               size_t *n_prio,
                                               size_t start_prio,
                                               size_t max_prio,
                                               TarotCard * prio,
                                               size_t *n_additional,
                                               size_t start_additional,
                                               size_t max_additional,
                                               TarotCard * additional);
  static TarotGameError game_get_hint_card (const TarotGame * game,
                                            TarotPlayer * player, size_t *n,
                                            size_t start, size_t max,
                                            TarotCard * playable);
  static int game_check_event (const TarotGame * game,
                               const TarotGameEvent * event);
  static int game_check_card (const TarotGame * game, TarotCard card,
                              int *cannot_follow, TarotSuit * lead_suit,
                              int *cannot_overtrump, TarotNumber * max_trump,
                              int *cannot_undertrump);
  static TarotGameError game_add_event (TarotGame * game,
                                        const TarotGameEvent * event);
  static int game_can_autoreveal (const TarotGame * game, size_t *n_dog,
                                  size_t start, size_t max, TarotCard * dog);
  static int game_assign_missing_card (TarotGame * game, TarotCard card,
                                       TarotPlayer owner);
  static const TarotGameEvent *game_iterator_next_value (TarotGameIterator *
                                                         iterator);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_GAME_PRIVATE_INCLUDED */
