/*
 * file tarot/game.h libtarot header for the management of a game.
 *
 * Copyright (C) 2018 Vivien Kraus <vivien@planete-kraus.eu>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef H_TAROT_GAME_INCLUDED
#define H_TAROT_GAME_INCLUDED

#include <stddef.h>

#include <tarot/bid.h>
#include <tarot/player.h>
#include <tarot/step.h>
#include <tarot/cards.h>
#include <tarot/game_event.h>

#ifdef __cplusplus
extern "C"
{
#endif                          /* __cplusplus */

  struct TarotGame;
  typedef struct TarotGame TarotGame;

  struct TarotGameIterator;
  typedef struct TarotGameIterator TarotGameIterator;

  typedef enum
  {
    TAROT_GAME_OK = 0,
    TAROT_GAME_INVXML,
    TAROT_GAME_INVEV,
    TAROT_GAME_NA
  } TarotGameError;

  /**
   * tarot_game_alloc: (constructor)
   */
  TarotGame *tarot_game_alloc (void);
  TarotGame *tarot_game_dup (const TarotGame * source);
  void tarot_game_free (TarotGame * game);

  /**
   * tarot_game_construct:
   * @max_mem: available number of bytes in @mem
   * @mem: (array length=max_mem): available aligned memory
   * @alignment: (out): the game alignment
   */
  size_t tarot_game_construct (size_t max_mem, char *mem, size_t *alignment);

  void tarot_game_copy (TarotGame * dest, const TarotGame * source);

  TarotStep tarot_game_step (const TarotGame * game);

  /**
   * tarot_game_iterator_construct:
   * @mem: (array length=max_mem):
   * @alignment: (out):
   */
  size_t tarot_game_iterator_construct (size_t max_mem,
                                        char *mem,
                                        size_t *alignment,
                                        const TarotGame * game);

  TarotGameIterator *tarot_game_iterator (const TarotGame * game);

  TarotGameIterator *tarot_game_iterator_dup
    (const TarotGameIterator * source);

  void tarot_game_iterator_free (TarotGameIterator * game_iterator);

  /**
   * tarot_game_save_to_xml:
   * @dest: (array length=max):
   */
  size_t tarot_game_save_to_xml (const TarotGame * game, size_t start,
                                 size_t max, char *dest);

  /**
   * tarot_game_save_to_xml_alloc: (rename-to tarot_game_save_to_xml)
   */
  char *tarot_game_save_to_xml_alloc (const TarotGame * game);

  /**
   * tarot_game_load_from_xml:
   */
  TarotGameError tarot_game_load_from_xml (TarotGame * game,
                                           const char *input);

  size_t tarot_game_n_players (const TarotGame * game);

  /**
   * tarot_game_with_call:
   * Returns: (type boolean):
   */
  int tarot_game_with_call (const TarotGame * game);

  /**
   * tarot_game_get_next:
   * @next: (out):
   */
  TarotGameError tarot_game_get_next (const TarotGame * game,
                                      TarotPlayer * next);

  /**
   * tarot_game_get_main_player:
   * @main: (out):
   */
  TarotGameError tarot_game_get_main_player (const TarotGame * game,
                                             TarotPlayer * main);

  /**
   * tarot_game_get_deal_all:
   * @owners: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_deal_all (const TarotGame * game, size_t *n,
                                          size_t start, size_t max,
                                          TarotPlayer * owners);

  /**
   * tarot_game_get_deal_all_alloc: (rename-to tarot_game_get_deal_all)
   * @owners: (out) (array length=n):
   */
  TarotGameError tarot_game_get_deal_all_alloc (const TarotGame * game,
                                                size_t *n,
                                                TarotPlayer ** owners);

  /**
   * tarot_game_get_deal_of:
   * @deal: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_deal_of (const TarotGame * game,
                                         TarotPlayer who, size_t *n,
                                         size_t start, size_t max,
                                         TarotCard * deal);

  /**
   * tarot_game_get_deal_of_alloc: (rename-to tarot_game_get_deal_of)
   * @cards: (out) (array length=n):
   */
  TarotGameError tarot_game_get_deal_of_alloc (const TarotGame * game,
                                               TarotPlayer who, size_t *n,
                                               TarotCard ** cards);

  /**
   * tarot_game_get_cards:
   * @cards: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_cards (const TarotGame * game,
                                       TarotPlayer who, size_t *n,
                                       size_t start, size_t max,
                                       TarotCard * cards);

  /**
   * tarot_game_get_cards_alloc: (rename-to tarot_game_get_cards)
   * @cards: (out) (array length=n):
   */
  TarotGameError tarot_game_get_cards_alloc (const TarotGame * game,
                                             TarotPlayer who, size_t *n,
                                             TarotCard ** cards);

  /**
   * tarot_game_get_bids:
   * @bids: (array length=max):
   */
  TarotGameError tarot_game_get_bids (const TarotGame * game, size_t *n,
                                      TarotPlayer start, size_t max,
                                      TarotBid * bids);

  /**
   * tarot_game_get_bids_alloc: (rename-to tarot_game_get_bids)
   * @bids: (out) (array length=n):
   */
  TarotGameError tarot_game_get_bids_alloc (const TarotGame * game, size_t *n,
                                            TarotBid ** bids);

  /**
   * tarot_game_get_taker:
   * @taker: (out):
   */
  TarotGameError tarot_game_get_taker (const TarotGame * game,
                                       TarotPlayer * taker);

  /**
   * tarot_game_get_declarations:
   * @decls: (array length=max) (type boolean):
   */
  TarotGameError tarot_game_get_declarations (const TarotGame * game,
                                              size_t *n, TarotPlayer start,
                                              size_t max, int *decls);

  /**
   * tarot_game_get_declarations_alloc: (rename-to tarot_game_get_declarations)
   * @declarations: (out) (array length=n) (element-type boolean):
   */
  TarotGameError tarot_game_get_declarations_alloc (const TarotGame * game,
                                                    size_t *n,
                                                    int **declarations);

  /**
   * tarot_game_get_declarant:
   * @declarant: (out):
   */
  TarotGameError tarot_game_get_declarant (const TarotGame * game,
                                           TarotPlayer * declarant);

  /**
   * tarot_game_get_call:
   * @card: (out):
   */
  TarotGameError tarot_game_get_call (const TarotGame * game,
                                      TarotCard * card);

  /**
   * tarot_game_get_partner:
   * @partner: (out):
   */
  TarotGameError tarot_game_get_partner (const TarotGame * game,
                                         TarotPlayer * partner);

  /**
   * tarot_game_get_dog:
   * @cards: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_dog (const TarotGame * game, size_t *n,
                                     size_t start, size_t max,
                                     TarotCard * cards);

  /**
   * tarot_game_get_dog_alloc: (rename-to tarot_game_get_dog)
   * @cards: (out) (array length=n_cards):
   */
  TarotGameError tarot_game_get_dog_alloc (const TarotGame * game,
                                           size_t *n_cards,
                                           TarotCard ** cards);

  /**
   * tarot_game_get_full_discard:
   * @cards: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_full_discard (const TarotGame * game,
                                              size_t *n, size_t start,
                                              size_t max, TarotCard * cards);

  /**
   * tarot_game_get_full_discard_alloc:
   * @cards: (out) (array length=n):
   */
  TarotGameError tarot_game_get_full_discard_alloc (const TarotGame * game,
                                                    size_t *n,
                                                    TarotCard ** cards);

  /**
   * tarot_game_get_public_discard:
   * @cards: (array length=max):
   * @n: (out):
   */
  TarotGameError tarot_game_get_public_discard (const TarotGame * game,
                                                size_t *n, size_t start,
                                                size_t max,
                                                TarotCard * cards);

  /**
   * tarot_game_get_public_discard_alloc:
   * @cards: (out) (array length=n):
   */
  TarotGameError tarot_game_get_public_discard_alloc (const TarotGame * game,
                                                      size_t *n,
                                                      TarotCard ** cards);

  /**
   * tarot_game_get_handful:
   * @size: (out):
   */
  TarotGameError tarot_game_get_handful (const TarotGame * game,
                                         TarotPlayer p, int *size,
                                         size_t *n_cards, size_t start,
                                         size_t max, TarotCard * cards);

  /**
   * tarot_game_get_handful_alloc: (rename-to tarot_game_get_handful)
   * @size: (out):
   * @cards: (out) (array length=n_cards):
   */
  TarotGameError tarot_game_get_handful_alloc (const TarotGame * game,
                                               TarotPlayer p, int *size,
                                               size_t *n_cards,
                                               TarotCard ** cards);

  /**
   * tarot_game_n_tricks:
   */
  size_t tarot_game_n_tricks (const TarotGame * game);

  /**
   * tarot_game_get_current_trick:
   * @current: (out):
   */
  TarotGameError tarot_game_get_current_trick (const TarotGame * game,
                                               size_t *current);

  /**
   * tarot_game_get_lead_suit:
   * @lead_suit: (out):
   */
  TarotGameError tarot_game_get_lead_suit (const TarotGame * game,
                                           size_t i_trick,
                                           TarotSuit * lead_suit);

  /**
   * tarot_game_get_max_trump:
   * @max_trump: (out):
   */
  TarotGameError tarot_game_get_max_trump (const TarotGame * game,
                                           size_t i_trick,
                                           TarotNumber * max_trump);

  /**
   * tarot_game_get_trick_leader:
   * @leader: (out):
   */
  TarotGameError tarot_game_get_trick_leader (const TarotGame * game,
                                              size_t i_trick,
                                              TarotPlayer * leader);

  /**
   * tarot_game_get_trick_taker:
   * @taker: (out):
   */
  TarotGameError tarot_game_get_trick_taker (const TarotGame * game,
                                             size_t i_trick,
                                             TarotPlayer * taker);

  struct TarotGameCardAnalysis
  {
    TarotCard card;
    int cannot_follow;
    int cannot_overtrump;
    int cannot_undertrump;
  };
  typedef struct TarotGameCardAnalysis TarotGameCardAnalysis;

  /**
   * tarot_game_get_trick_cards:
   * @n_cards: (out):
   * @cards: (array length=max):
   */
  TarotGameError tarot_game_get_trick_cards (const TarotGame * game,
                                             size_t i_trick,
                                             size_t *n_cards, size_t start,
                                             size_t max,
                                             TarotGameCardAnalysis * cards);

  /**
   * tarot_game_get_trick_cards_alloc: (rename-to tarot_game_get_trick_cards)
   * @cards: (out) (array length=n_cards):
   */
  TarotGameError tarot_game_get_trick_cards_alloc
    (const TarotGame * game,
     size_t i_trick, size_t *n_cards, TarotGameCardAnalysis ** cards);

  /**
   * tarot_game_get_points_in_trick:
   * @halfpoints: (out):
   * @oudlers: (out):
   */
  TarotGameError tarot_game_get_points_in_trick (const TarotGame * game,
                                                 size_t i_trick,
                                                 unsigned int *halfpoints,
                                                 unsigned int *oudlers);

  /**
   * tarot_game_get_scores:
   * @scores: (array length=max):
   */
  TarotGameError tarot_game_get_scores (const TarotGame * game, size_t *n,
                                        size_t start, size_t max,
                                        int *scores);

  /**
   * tarot_game_get_scores_alloc: (rename-to tarot_game_get_scores)
   * @scores: (out) (array length=n):
   */
  TarotGameError tarot_game_get_scores_alloc (const TarotGame * game,
                                              size_t *n, int **scores);

  /**
   * tarot_game_get_hint_bid:
   * @player: (out):
   * @mini: (out):
   */
  TarotGameError tarot_game_get_hint_bid (const TarotGame * game,
                                          TarotPlayer * player,
                                          TarotBid * mini);

  /**
   * tarot_game_get_hint_decl:
   * @player: (out):
   * @allowed: (out) (type boolean):
   */
  TarotGameError tarot_game_get_hint_decl (const TarotGame * game,
                                           TarotPlayer * player,
                                           int *allowed);

  /**
   * tarot_game_get_hint_call:
   * @player: (out):
   * @mini: (out):
   */
  TarotGameError tarot_game_get_hint_call (const TarotGame * game,
                                           TarotPlayer * player,
                                           TarotNumber * mini);

  /**
   * tarot_game_get_hint_discard:
   * @player: (out):
   * @n_cards: (out):
   * @n_prio: (out):
   * @n_additional: (out):
   * @prio: (array length=max_prio):
   * @additional: (array length=max_additional):
   */
  TarotGameError tarot_game_get_hint_discard (const TarotGame * game,
                                              TarotPlayer * player,
                                              size_t *n_cards,
                                              size_t *n_prio,
                                              size_t start_prio,
                                              size_t max_prio,
                                              TarotCard * prio,
                                              size_t *n_additional,
                                              size_t start_additional,
                                              size_t max_additional,
                                              TarotCard * additional);

  /**
   * tarot_game_get_hint_discard_alloc: (rename-to tarot_game_get_hint_discard)
   * @player: (out):
   * @n_cards: (out):
   * @prio: (out) (array length=n_prio):
   * @additional: (out) (array length=n_additional):
   */
  TarotGameError tarot_game_get_hint_discard_alloc (const TarotGame * game,
                                                    TarotPlayer * player,
                                                    size_t *n_cards,
                                                    size_t *n_prio,
                                                    TarotCard ** prio,
                                                    size_t *n_additional,
                                                    TarotCard ** additional);

  /**
   * tarot_game_get_hint_handful:
   * @player: (out):
   * @n_simple: (out):
   * @n_double: (out):
   * @n_triple: (out):
   * @n_prio: (out):
   * @n_additional: (out):
   * @prio: (array length=max_prio):
   * @additional (array length=max_additional):
   */
  TarotGameError tarot_game_get_hint_handful (const TarotGame * game,
                                              TarotPlayer * player,
                                              size_t *n_simple,
                                              size_t *n_double,
                                              size_t *n_triple,
                                              size_t *n_prio,
                                              size_t start_prio,
                                              size_t max_prio,
                                              TarotCard * prio,
                                              size_t *n_additional,
                                              size_t start_additional,
                                              size_t max_additional,
                                              TarotCard * additional);

  /**
   * tarot_game_get_hint_handful_alloc: (rename-to tarot_game_get_hint_handful)
   * @player: (out):
   * @n_simple: (out):
   * @n_double: (out):
   * @n_triple: (out):
   * @prio: (out) (array length=n_prio):
   * @additional: (out) (array length=n_additional):
   */
  TarotGameError tarot_game_get_hint_handful_alloc (const TarotGame * game,
                                                    TarotPlayer * player,
                                                    size_t *n_simple,
                                                    size_t *n_double,
                                                    size_t *n_triple,
                                                    size_t *n_prio,
                                                    TarotCard ** prio,
                                                    size_t *n_additional,
                                                    TarotCard ** additional);

  /**
   * tarot_game_get_hint_card:
   * @player: (out):
   * @n: (out):
   * @playable: (array length=max):
   */
  TarotGameError tarot_game_get_hint_card (const TarotGame * game,
                                           TarotPlayer * player,
                                           size_t *n,
                                           size_t start,
                                           size_t max, TarotCard * playable);

  /**
   * tarot_game_get_hint_card_alloc: (rename-to tarot_game_get_hint_card)
   * @player: (out):
   * @playable: (out) (array length=n_playable):
   */
  TarotGameError tarot_game_get_hint_card_alloc (const TarotGame * game,
                                                 TarotPlayer * player,
                                                 size_t *n_playable,
                                                 TarotCard ** playable);

  /**
   * tarot_game_check_event:
   * Returns: (type boolean):
   */
  int tarot_game_check_event (const TarotGame * game,
                              const TarotGameEvent * event);

  /**
   * tarot_game_check_card:
   * @cannot_follow: (type boolean) (out):
   * @lead_suit: (out):
   * @cannot_overtrump: (type boolean) (out):
   * @max_trump: (out):
   * @cannot_undertrump: (type boolean) (out):
   * Returns: (type boolean):
   */
  int tarot_game_check_card (const TarotGame * game, TarotCard card,
                             int *cannot_follow, TarotSuit * lead_suit,
                             int *cannot_overtrump, TarotNumber * max_trump,
                             int *cannot_undertrump);

  TarotGameError tarot_game_add_event (TarotGame * game,
                                       const TarotGameEvent * event);

  /**
   * tarot_game_can_autoreveal:
   * @n_dog: (out):
   * @dog: (array length=max):
   * Returns: (type boolean):
   */
  int tarot_game_can_autoreveal (const TarotGame * game, size_t *n_dog,
                                 size_t start, size_t max, TarotCard * dog);

  /**
   * tarot_game_can_autoreveal_alloc: (rename-to tarot_game_can_autoreveal)
   * @cards: (out) (array length=n):
   * Returns: (type boolean):
   */
  int tarot_game_can_autoreveal_alloc (const TarotGame * game, size_t *n,
                                       TarotCard ** cards);

  /* WARNING: This function may succeed but lead to a state where a
     player has no playable card!!  Don't use this directly, rather
     use tarot_counter_and_game_impute. */
  int tarot_game_assign_missing_card (TarotGame * game, TarotCard card,
                                      TarotPlayer owner);

  /**
   * tarot_game_iterator_next_value:
   * Returns: (nullable):
   */
  const TarotGameEvent *tarot_game_iterator_next_value (TarotGameIterator *
                                                        iterator);

#ifdef __cplusplus
}
#endif                          /* __cplusplus */

#endif                          /* not H_TAROT_GAME_INCLUDED */
